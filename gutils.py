import secrets
from collections import deque
import multiprocessing
from bitstring import BitArray
import progressbar
import librosa, librosa.display
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import IPython.display as ipd
import numpy as np
import pandas as pd
import sklearn
from sklearn import preprocessing
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from skimage.transform import rescale, resize, downscale_local_mean
import cv2
from glob import glob
from sys import exit
# from time import time
import time
import shutil
import os
import scipy
from scipy import signal
from scipy import stats
# from scipy.misc import imsave
import datetime
import math
from pathlib import Path
import youtube_dl
import argparse
import sys
import mido
import json
import os.path
from os import path
import seaborn as sns
import copy
import PIL
from scipy import misc

import random

from madmom.audio.filters import LogarithmicFilterbank
from madmom.features.onsets import SpectralOnsetProcessor
from madmom.audio.signal import normalize

# import parselmouth

from midiutil.MidiFile import MIDIFile
from pandas.core.frame import DataFrame

from multiprocessing import Pool
import multiprocessing as mp

from channel import Channel
from chord import Chord

sns.set() # Use seaborn's default style to make attractive graphs

###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################        
class GUtils(Channel,Chord):        
    def __init__(self):
        pass

###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################         
    def init_vars(self, shift=True, hop_length=512, sample_rate=20442,
                  time_steps=0.1, pixel_interval=0.025, isTrain=True, renew_all=True, light_pixel_interval=0.025,verbose=False,total_channels=6):
        self.init_patterns(total_channels)
        self.hop_length = hop_length
        self.sample_rate = sample_rate
        self.time_steps = time_steps
        self.shift = shift
        self.tempo = 120
        self.pixel_interval=pixel_interval
        self.time_interval=self.pixel_interval*256
        self.isTrain = isTrain
        self.renew_all = renew_all
        self.light_pixel_interval = light_pixel_interval
        self.verbose = verbose
        print("hop_length=%d, sample_rate=%s, time_steps=%f, shift=%d, pixel_interval=%f, time_interval=%f, isTrain=%d, renew_all=%d, verbose=%d" 
              %(self.hop_length,
                self.sample_rate,
                self.time_steps,
                self.shift,
                self.pixel_interval,
                self.time_interval,
                self.isTrain,
                self.renew_all,self.verbose))    
                
        # self.df_theorytab_symble = pd.read_csv('theorytab_symble.csv')
        # self.df_theorytab_structure = pd.read_csv('theorytab_structure.csv')
        # self.theorytab_symble_dict = {}
        # for index, row in self.df_theorytab_symble.iterrows():
        #     self.theorytab_symble_dict['%s'%(row['chord'])] = row['code']
            
        # self.theorytab_structure_dict = {}
        # for index, row in self.df_theorytab_structure.iterrows():
        #     self.theorytab_structure_dict['%s'%(row['structure'])] = row['code']
        
        self.df_symble = pd.read_csv('chord_symble.csv')
        self.symble_dict = {}
        for index, row in self.df_symble.iterrows():
            self.symble_dict['%s'%(row['chord'])] = row['code']
###############################################################################################
###############################################################################################    
###############################################################################################
############################################################################################### 
    def rgb2gray(self, rgb):
        return np.dot(rgb[...,:3], [0.299, 0.587, 0.114])  

###############################################################################################
###############################################################################################    
###############################################################################################
############################################################################################### 
    def init_patterns(self, total_channels=6, pattern_path = './pattern'):
        self.NOTES_FLAT = ['C', 'Db', 'D', 'Eb', 'E', 'F', 'Gb', 'G', 'Ab', 'A', 'Bb', 'B']     
        self.PATTERN_NOTES_CODE_DICT = {
            'C' :'01', 
            'Db':'02', 
            'D' :'04', 
            'Eb':'08', 
            'E' :'10', 
            'F' :'20', 
            'Gb':'15', 
            'G' :'25', 
            'Ab':'1A', 
            'A' :'2A', 
            'Bb':'21', 
            'B' :'3F', 
        }
        self.secretsGenerator = secrets.SystemRandom()   
        self.pattern_path = pattern_path
        self.total_channels = total_channels
        self.channel_max_code = 2 ** self.total_channels - 1
        # self.direction_list = ['Down_0','Down_1','Up_0','Up_1']
        self.direction_list = ['Down_0','Up_0']
        self.effects_dict = {}
        for i in range(self.total_channels):
            self.effects_dict['ch%d'%(i)] = {
                # 'direction' : self.secretsGenerator.sample(self.direction_list, k=1)[0],
                # 'step' : int(secrets.token_hex(1),16) & 3,
                'code' : 1 << i,
            }
        self.effects_dict['pattern_code'] = 0 # int(secrets.token_hex(1),16) & self.channel_max_code
        self.effects_dict['pre_pattern_code'] = 0
        self.effects_dict['direction'] = self.secretsGenerator.sample(self.direction_list, k=1)[0]
        self.effects_dict['step'] = 1 # int(secrets.token_hex(1),16) & 3
        self.effects_dict['pattern_name'] = []
        self.effects_dict['pre_pattern_name'] = []
        self.effects_dict['decreasing'] = float(0.4)
        self.effects_dict['random'] = 1
        self.effects_dict['pitch'] = 0
        self.effects_dict['pre_pitch'] = 0

        self.test_code = [
            int('000001', 2),
            int('000010', 2),
            int('000100', 2),
            int('001000', 2),
            int('010000', 2),
            int('100000', 2),
            int('100001', 2),
            int('010010', 2),
            int('001100', 2),
            int('010010', 2),
            int('100001', 2),
            int('001100', 2),
            int('000001', 2),
            int('100000', 2),
            int('000001', 2),
            int('100000', 2),
            int('110000', 2),
            int('000011', 2),
            int('000111', 2),
            int('111000', 2),
            int('000111', 2),
            int('000011', 2)]
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################
    def reverseBits(self,num,bitSize): 
        # convert number into binary representation 
        # output will be like bin(10) = '0b10101' 
        binary = bin(num) 
    
        # skip first two characters of binary 
        # representation string and reverse 
        # remaining string and then append zeros 
        # after it. binary[-1:1:-1]  --> start 
        # from last character and reverse it until 
        # second last character from left 
        reverse = binary[-1:1:-1] 
        reverse = reverse + (bitSize - len(reverse))*'0'
    
        # converts reversed binary string into integer 
    #     print (int(reverse,2))
        # print(reverse)
        return int(reverse,2)
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################
    def shift_pattern_code(self,direction,shift=True):
        self.effects_dict['pattern_code'] = 0
        if shift == False:
            return
        for i in range(self.total_channels):
            if direction == 'Down_0': # shift left pad 0                           
                self.effects_dict['ch%d'%(i)]['code'] = (self.effects_dict['ch%d'%(i)]['code'] << self.effects_dict['step']) & self.channel_max_code
            #==================================================================
            elif direction == 'Up_0': # shift right pad 0 
                self.effects_dict['ch%d'%(i)]['code'] = (self.effects_dict['ch%d'%(i)]['code'] >> self.effects_dict['step']) & self.channel_max_code
            #==================================================================
            elif direction == 'Down_1': # shift left pad 1
                self.effects_dict['ch%d'%(i)]['code'] = (self.effects_dict['ch%d'%(i)]['code'] << self.effects_dict['step']) & self.channel_max_code
                self.effects_dict['ch%d'%(i)]['code'] |= 1
            #==================================================================
            elif direction == 'Up_1': # shift right pad 1 
                self.effects_dict['ch%d'%(i)]['code'] = (self.effects_dict['ch%d'%(i)]['code'] >> self.effects_dict['step']) & self.channel_max_code
                self.effects_dict['ch%d'%(i)]['code'] |= (1<<(self.total_channels-1))
            #==================================================================
            elif direction == 'ReverseBits': 

                self.effects_dict['ch%d'%(i)]['code'] = self.reverseBits(self.effects_dict['ch%d'%(i)]['code'], bitSize=self.total_channels)
            #==================================================================
            self.effects_dict['pattern_code'] |= self.effects_dict['ch%d'%(i)]['code']

###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################
    def set_pattern_code(self, code, direction, step):
        self.effects_dict['direction'] = direction
        self.effects_dict['step'] = step 
        self.effects_dict['pattern_code'] = code
        for i in range(self.total_channels):
            self.effects_dict['ch%d'%(i)]['code'] = code & (1 << i)
        self.effects_dict['pre_pattern_code'] = code
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################
    def random_pattern_code(self,):
        while True:
            new_code = int(secrets.token_hex(1),16) & self.channel_max_code
            if new_code != 0:
                self.set_pattern_code(code=new_code, direction=self.secretsGenerator.sample(self.direction_list, k=1)[0], step=1)
                return new_code
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################
    def init_pattern_code(self, code):
        if code == 0:# or code == self.channel_max_code:
            while True:
                new_code = int(secrets.token_hex(1),16) & self.channel_max_code                
                if new_code != 0:
                    direction = []
                    if new_code == 1:
                        direction = 'Down_0'
                    elif new_code == (1 << (self.channel_max_code-1)):    
                        direction = 'Up_0'
                    else:
                        direction=self.secretsGenerator.sample(self.direction_list, k=1)[0]
                    self.set_pattern_code(code=new_code, direction=direction, step=1)
                    return new_code
        self.effects_dict['pre_pattern_code'] = code
        return code
###############################################################################################
###############################################################################################    
###############################################################################################
############################################################################################### 
    def select_patterns(self,total_channels,unit_steps,pattern_type,shift=True):
        self.effects_dict['pattern_name'] = []
        ###########################################################################################################
        if pattern_type == 'L':
            new_code = self.init_pattern_code(self.effects_dict['pre_pattern_code'])         
            datasetsPath = Path(self.pattern_path)
            # links_path = datasetsPath.glob('**/S%02X_E*_L*.png'%(new_code))
            links_path = datasetsPath.glob('**/S*_E*_L*.png')
            L_list = []
            for link in links_path:
                END_STRING = str(link).split('_', 2)[1][1:]
                if self.verbose:
                    print('[L] ',link,END_STRING, int(END_STRING, 16))
                if int(END_STRING, 16) & self.channel_max_code != 0:
                    L_list.append(link)
            L_list = shuffle(L_list)
            pattern_basename = os.path.basename(self.secretsGenerator.sample(L_list, k=1)[0])
            self.effects_dict['pattern_name'] = os.path.splitext(pattern_basename)[0]
            END_STR = self.effects_dict['pattern_name'].split('_', 2)[1][1:]
            self.set_pattern_code(code=int(END_STR, 16), direction=self.secretsGenerator.sample(self.direction_list, k=1)[0], step=1)
            self.effects_dict['decreasing'] = 10
        ###########################################################################################################
        elif pattern_type == 'M':
            # print('[M] ', self.test_code)
            # self.effects_dict['pattern_name'] = 'S%02X_E%02X_M'%(self.test_code[0], self.test_code[0])
            # self.test_code = np.roll(self.test_code,-1)
            # print('[M] ', self.test_code)
            code = self.init_pattern_code(code = self.effects_dict['pattern_code'])
            code = 63
            self.effects_dict['pattern_name'] = 'S%02X_E%02X_M'%(code, code)
            self.shift_pattern_code(shift=shift, direction=self.effects_dict['direction'])
            self.effects_dict['decreasing'] = 10
        ###########################################################################################################
        elif pattern_type == 'S':            
            # print('[S] ',self.items.rotate(1))
            # code = self.items.rotate(1)[0] 
            # code = self.init_pattern_code(code = self.effects_dict['pattern_code'])
            # code = 0
            # print('[S] ', self.test_code)
            # self.effects_dict['pattern_name'] = 'S%02X_E%02X_M'%(self.test_code[0], self.test_code[0])
            # self.test_code = np.roll(self.test_code,-1)
            # # code = self.test_code[0]
            # print('[S] ', self.test_code)
            code = self.init_pattern_code(code = self.effects_dict['pattern_code'])
            code = 63
            self.effects_dict['pattern_name'] = 'S%02X_E%02X_M'%(code, code)
            self.shift_pattern_code(shift=shift, direction=self.effects_dict['direction'])
            self.effects_dict['decreasing'] = 10
        ##########################################################################################################    
        elif pattern_type == 'party':
            pass
        ##########################################################################################################    
        if self.verbose:
            print(' [pattern info] [%s] [now:%s] [pre:%s] [direction:%s] [step:%d]'%(pattern_type, 
                self.effects_dict['pattern_name'], self.effects_dict['pre_pattern_name'], self.effects_dict['direction'], self.effects_dict['step']))
        
        an_image = PIL.Image.open('%s/%s.png'%(self.pattern_path, self.effects_dict['pattern_name']))
        an_image_sz = an_image.resize( (unit_steps, total_channels),  PIL.Image.NEAREST )
        new_grayscale_image_sz = an_image_sz.convert("L")
        new_grayscale_array_sz = np.asarray(new_grayscale_image_sz).astype('float32')
        # new_grayscale_array_sz = new_grayscale_array_sz // 100
        # new_grayscale_array_sz = new_grayscale_array_sz // 50.0
        if self.verbose:
            self.format_display(new_grayscale_array_sz)

        self.effects_dict['pre_pattern_name'] = self.effects_dict['pattern_name']
        return new_grayscale_array_sz  
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################
    def check_key_exist(self,test_dict, key):
        try:
            value = test_dict[key]
            return True
        except KeyError:
            return False      
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################
    def api_note2light(self, basename, note_list, pattern_dict, struct_onset_list, dataroot='./light', 
                        pattern_mode='normal', total_channels=6): 
        import time as ts
        startTime = ts.time()     
        channels_dict = dict()

        # datasetsPath = Path(self.pattern_path)
        # if pattern_dict == None:
        #     pattern_dict = dict()
        #     for r in rule: 
        #         pattern_dict[r] = []  
        #         L_LIST = [] 
        #         links_path = datasetsPath.glob('**/%s*.png'%(r))
        #         for link in links_path:
        #             L_LIST.append(link)
        #         pattern_dict[r] = deque(shuffle(L_LIST))
        # else:
        #     for r in pattern_dict: 
        #         pattern_dict[r] = deque(pattern_dict[r])
        # pattern_dict_temp = copy.deepcopy(pattern_dict)
        
        # for r in rule:             
        #     channels_dict[r] = dict()            
        #     for i in range(total_channels):                
        #         channels_dict[r]['ch%d'%(i+1)] = []
        print(' ================= api_note2light pattern info ================ ')
        for key, value in pattern_dict.items():
            print(key,value['size'])
            for p in value['pattern']:
                value['pattern'][p] = deque(value['pattern'][p])
                channels_dict[p] = dict()  
                for i in range(total_channels):                
                    channels_dict[p]['ch%d'%(i+1)] = []
        pattern_dict_temp = copy.deepcopy(pattern_dict)

        pattern_name_dict = {
            'pattern_1':[],
            'pattern_2':[],
            'pattern_3':[],
            'pattern_4':[],
            'pattern_5':[],
            'pattern_6':[],
            'pattern_7':[],
            'pattern_8':[],
            'pattern':[],
        }
         

        # print(channels_dict)
        # print(pattern_dict)
    
        self.total_channels = total_channels
        self.channel_max_code = 2 ** self.total_channels - 1      
        light_start_time = 0.0
        light_end_time = note_list[-1][1]
        vocal_start_time = note_list[0][0]
        vocal_end_time = note_list[-1][1]
        note_index = 0
        note_counts = 0
        nT = np.array(note_list).T        
        max_pitch = np.max(nT[2])
        min_pitch = np.min(nT[2])

        df_pitch_info = pd.DataFrame({'note':nT[2],'time':np.array(nT[1])-np.array(nT[0])}) 
        pitch_time_25 = df_pitch_info.describe().loc[['25%']]['time'][0]
        pitch_time_50 = df_pitch_info.describe().loc[['50%']]['time'][0]
        pitch_time_75 = df_pitch_info.describe().loc[['75%']]['time'][0]
        
        print(' ================= api_note2light info ================ ')
        print(' Pattern Mode: %s'%(pattern_mode))
        print(' Light total time: %.3f <-> %.3f'%(light_start_time,light_end_time))
        print(' Note total time: %.3f <-> %.3f'%(vocal_start_time,vocal_end_time))      
        # print(' Pitch: %.3f <-> %.3f'%(min_pitch,max_pitch))     
        # print(df_pitch_info.describe())
        print(' 25%%:%.3f 50%%:%.3f 75%%:%.3f'%(pitch_time_25, pitch_time_50, pitch_time_75))
        print(' ================================================== ')     
        ##################################################################################### 
        #################################### Normal ######################################### 
        ##################################################################################### 
        if pattern_mode == 'normal':
            """
            ==========================================
            vocal   : 一般模式
            pattern : 藉由音長區間，自動搭配合適的圖驣
            ==========================================
            """       
            bSel = True
            light_time = light_start_time
            new_level = 0
            self.effects_dict['pitch'] = round(note_list[note_index][2],3)
            self.effects_dict['pre_pitch'] = round(note_list[note_index][2],3)-1
            struct_onset_list_temp = copy.deepcopy(struct_onset_list)
            pattern_type = []
            obj_struct = struct_onset_list.pop(0)
            struct_onset_time = obj_struct[0]
            struct_name = obj_struct[1]
            
            while True:                
                onset_time = note_list[note_index][0]   # sec
                offset_time = note_list[note_index][1]  # sec
                duration_time = offset_time-onset_time  # sec
                decreasing_step = duration_time/self.light_pixel_interval
                unit_steps = int(math.ceil(duration_time/self.light_pixel_interval))
                chord_int = round(note_list[note_index][2],3)

                if unit_steps == 0:
                    unit_steps = 1
                # print(onset_time,offset_time,unit_steps,self.light_pixel_interval)

                if struct_onset_list != []:
                    if light_time >= struct_onset_list[0][0]:
                        obj_struct = struct_onset_list.pop(0)
                        struct_onset_time = obj_struct[0]
                        struct_name = obj_struct[1]


                if bSel:
                    if self.verbose:
                        print('[light info] [pattern:%s] prev_pitch:%.3f(%s%d) pitch:%.3f(%s%d) duration_time:%.3f'%(
                            self.effects_dict['pattern_name'],duration_time))                    
                    bSel = False                  
                    pattern_array = []
                    
                    for _s,_t in pattern_dict[struct_name]['size'].items():
                        if duration_time >= _t:
                            pattern_type = _s

                            path_name = str(pattern_dict[struct_name]['pattern'][pattern_type][0])
                            dir_name = os.path.dirname(path_name)
                            base_name = os.path.basename(path_name)
                            
                            datasetsPath = Path(dir_name)
                            # links_path = datasetsPath.glob('**/*.mp3')
                            links_path = datasetsPath.glob(base_name)
                            pattern_path_list = []
                            for link in links_path:  
                                pattern_path_list.append(link)

                            # pattern_path_list=self.secretsGenerator.shuffle(pattern_path_list)
                            sample_pattern_path_list=self.secretsGenerator.sample(pattern_path_list, k=1)
                            
                            #### Rodom None repeated selection ####
                            pattern_dirname = os.path.dirname(sample_pattern_path_list[0])
                            split_name = os.path.splitext(pattern_dirname)[0]
                            if split_name == 'pattern_1' or  split_name == 'pattern_2'or \
                                split_name == 'pattern_3' or split_name == 'pattern_4' or\
                                split_name == 'pattern_5'or split_name == 'pattern_6':
                                # print(split_name,pattern_name_dict[split_name] , sample_pattern_path_list[0])
                                if pattern_name_dict[split_name] != [] and pattern_name_dict[split_name] == sample_pattern_path_list[0]:
                                    while True:
                                        sample_pattern_path_list=self.secretsGenerator.sample(pattern_path_list, k=1)
                                        pattern_dirname = os.path.dirname(sample_pattern_path_list[0])
                                        split_name = os.path.splitext(pattern_dirname)[0]
                                        # print(pattern_name_dict[split_name],sample_pattern_path_list[0])
                                        if pattern_name_dict[split_name] != sample_pattern_path_list[0]:
                                            break
                                pattern_name_dict[split_name] = copy.deepcopy(sample_pattern_path_list[0])
                            

                            # pattern_path_list = shuffle(pattern_path_list) 
                            an_image = PIL.Image.open(sample_pattern_path_list[0])

                            # if self.check_key_exist(test_dict=pattern_dict[struct_name],key='filter'):
                            #     if self.check_key_exist(test_dict=pattern_dict[struct_name]['filter'],key=_s):
                            #         # print('Before:',pattern_array)                               
                            #         an_image = self.filter(src=np.float32(an_image), 
                            #             mode=pattern_dict[struct_name]['filter'][_s]['mode'], 
                            #             ksize=pattern_dict[struct_name]['filter'][_s]['ksize'])
                            #         # print('After:',pattern_array) 
                            #         an_image = an_image_sz.convert("P")

                            an_image_sz = an_image.resize( (unit_steps, self.total_channels),  PIL.Image.NEAREST )
                            # an_image_sz = an_image.resize( (unit_steps, self.total_channels),  PIL.Image.BICUBIC )
                            pattern_array = an_image_sz.convert("L")
                            pattern_array = np.asarray(pattern_array).astype('float32')
                            if self.check_key_exist(test_dict=pattern_dict[struct_name],key='rotate'):
                                if pattern_dict[struct_name]['rotate'][_s] == 'NO':
                                    pattern_dict[struct_name]['pattern'][_s].rotate(-1)
                            else:
                                pattern_dict[struct_name]['pattern'][_s].rotate(-1)
                            break 
                    
                    if self.check_key_exist(test_dict=pattern_dict[struct_name],key='rotate'):
                        for _s,_t in pattern_dict[struct_name]['size'].items():
                            if pattern_dict[struct_name]['rotate'][_s] == 'YES':
                                pattern_dict[struct_name]['pattern'][_s].rotate(-1)

        
                if light_time >= onset_time and light_time <= offset_time:
                    # if duration_time*1000 >= filter_noise_ms:
                    #     new_level = 255
                    # else:
                    #     new_level = 0
                  
                    for ch in range(total_channels):
                        # _new_level = new_level
                        # _new_level = _new_level * pattern_array[ch][note_counts] 

                        # _new_level = new_level
                        if chord_int == self.df_symble.shape[0]-1:
                             _new_level = 0
                        elif pattern_array[ch][note_counts] <= 50.0:
                            _new_level = 0
                        elif pattern_array[ch][note_counts] >= 255.0:
                            _new_level = 255
                        else:
                            _new_level = pattern_array[ch][note_counts]
                        
                        for r in pattern_dict[struct_name]['size']: 
                            if pattern_type == r:
                               channels_dict[r]['ch%d'%(ch+1)].append(int(_new_level))  
                            else:
                               channels_dict[r]['ch%d'%(ch+1)].append(int(0)) 
                    note_counts += 1
                    
                    if note_counts >= unit_steps:                        
                        note_counts = 0
                        note_index += 1
                        bSel = True
                        # print('[1-1] note_index:',note_index)

                elif light_time < onset_time:  
                    for ch in range(total_channels):
                        for r in pattern_dict[struct_name]['size']: 
                            channels_dict[r]['ch%d'%(ch+1)].append(int(0)) 

                elif light_time > offset_time:  
                    note_counts = 0
                    note_index += 1
                    bSel = True
                    continue

                if note_index >= len(note_list):
                    note_index = len(note_list)-1
                    
                light_time += self.light_pixel_interval
                if light_time > light_end_time:
                    break
                
            for ch in range(total_channels):
                for r in pattern_dict[struct_name]['size']: 
                    channels_dict[r]['ch%d'%(ch+1)].append(0.0) 

        ###########################
        #   Output Channels PNG   #
        ###########################
        for r in pattern_dict[struct_name]['size']: 
            # self.genChannelsPNG(dataroot, '%s_L'%(basename), channels_L)
            # self.genChannelsPNG(dataroot, '%s_M'%(basename), channels_M)
            # self.genChannelsPNG(dataroot, '%s_S'%(basename), channels_S)
            # self.genChannelsPNG(dataroot, '%s_%s'%(basename,r), channels_dict[r])
            self.genPNG(dataroot, '%s_%s'%(basename,r), channels_dict[r], m=1.0)

        print('[api_note2light] Done ... %.3fs'%(ts.time() - startTime))

###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################
    def filterPNG(self, basename, pattern_dict, struct_onset_list, dataroot='./light', total_channels=6):
        index = 1
        for onset,struct_name in struct_onset_list:
            # print(struct_name)
            if self.check_key_exist(test_dict=pattern_dict[struct_name],key='filter'):        
                for size,_k in pattern_dict[struct_name]['filter'].items():
                    # print(size,_k)
                    for kkk in _k:
                        # print(kkk)
                        if kkk == {}:
                            continue
                        image = PIL.Image.open('%s/%s_%s_channels.png'%(dataroot,basename,size))
                        _array = np.asarray(image).astype('float32')
                        # print(_array.shape)
                        stp = int(onset/0.025)
                        etp = _array.shape[1]
                        if index < len(struct_onset_list):
                            etp = int(struct_onset_list[index][0]/0.025)
                        # _array[:,stp:etp] = self.filter(_array[:,stp:etp], mode=pattern_dict[struct_name]['filter'][size]['mode'], ksize=pattern_dict[struct_name]['filter'][size]['ksize']) 
                        # kwargs = pattern_dict[struct_name]['filter'][size]
                        kwargs = kkk
                        print(stp,etp,kkk)
                        _array[:,stp:etp] = self.filter(_array[:,stp:etp], **kwargs) 
            #             print(_array.shape,stp,etp)
                        cv2.imwrite('%s/%s_%s_channels.png'%(dataroot,basename,size),_array)
            index += 1

###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################
    def format_display(self,array):
        h,w = array.shape
        for _h in range(h):
            _str = ''
            for _w in range(w):
                _str += '%3d,'%(array[_h][_w])
            print('%s'%(_str))
        
###############################################################################################
###############################################################################################    
###############################################################################################
############################################################################################### 
    def run_output_AB_features_datasets(self, imgs_MP3_Note,hFlip=False):
        start_time = datetime.datetime.now()
        imgs_MP3 = imgs_MP3_Note[0]
        imgs_Note = imgs_MP3_Note[1]
        counts = self.run_cut_full_image(imgs_MP3, imgs_Note, hFlip)        
        elapsed_time = datetime.datetime.now() - start_time
        if counts == 0:
            print('[ERROR] [A<B] A:%s B:%s counts:%d ---> [%s]' % (os.path.basename(imgs_MP3), os.path.basename(imgs_Note), counts, elapsed_time))
        else:
            print('A:%s B:%s counts:%d ---> [%s]' % (os.path.basename(imgs_MP3), os.path.basename(imgs_Note), counts, elapsed_time))

###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################                 
    def output_AB_features_datasets(self, csvFile):
        train = pd.read_csv(csvFile)
        imgs_MP3 = []
        imgs_Note = []
        for _path, _groundtruth, _songs in zip(train['paths'],train['groundtruth'],train['songs']):
            mp3_path = os.path.join(_path, _songs)
            basename = os.path.basename(mp3_path)
            imgname = '%s_feature_full.png'%(os.path.splitext(basename)[0])
            mp3_img_path = os.path.join(_path, imgname)

            groundtruth_path = os.path.join(_path, _groundtruth)
            basename = os.path.basename(groundtruth_path)
            imgname = '%s_%s.png'%(os.path.splitext(os.path.basename(mp3_path))[0],os.path.splitext(basename)[0])
            groundtruth_img_path = os.path.join(_path, imgname)

            if path.exists(mp3_img_path) == True and path.exists(groundtruth_img_path) == True:  
                imgs_MP3.append(mp3_img_path)
                imgs_Note.append(groundtruth_img_path)        
                print(mp3_img_path, groundtruth_img_path)
            else:
                print(" Not found MP3(%s) or groundtruth(%s)"%(mp3_img_path,groundtruth_img_path))
        try:
            shutil.rmtree(self.feature_file_path)
        except OSError as e:
            print(e)
        else:
            print("The %s directory is deleted successfully"%(self.feature_file_path))
            os.makedirs('%s' % (self.feature_file_path), exist_ok=True)    
            self.delete_ipynb_checkpoints()
            self.cut_full_image(imgs_MP3, imgs_Note)
        print("=== Done ===")
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################         
    def output_test_datasets(self, csvFile, who=None):
        test = pd.read_csv(csvFile)
        imgs_MP3 = []
        count = 1
        for _path, _songs in zip(test['paths'],test['songs']):
            mp3_path = os.path.join(_path, _songs)
            dirname = os.path.dirname(mp3_path)
            features_path = '%s/features'%(os.path.dirname(mp3_path))
            basename = os.path.basename(mp3_path)
            imgname = '%s_feature_full.png'%(os.path.splitext(basename)[0])
            mp3_img_path = os.path.join(_path, imgname)
            if path.exists(mp3_img_path) == True and self.renew_all == True:
                print('[%d] %s'%(count, mp3_img_path))
                imgs_MP3.append(mp3_img_path)
                count += 1
            elif path.exists(mp3_img_path) == True and self.renew_all == False and path.exists(features_path) == False:
                print('[%d] %s'%(count, mp3_img_path))
                imgs_MP3.append(mp3_img_path)
                count += 1
            else:
                print(" Not found MP3 %s"%(mp3_img_path))   
        if who == None:
            self.cut_features_image(imgs_MP3)
        else:
            self.cut_features_image([imgs_MP3[who]])
        print("=== Done ===")

###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################           
    def output_train_datasets(self, test_size=0.1, total_size=10000):
        import random
        startTime = time.time()
        image_path_list = []
        for file_path in glob('{}/*.png'.format(self.feature_file_path)):
            image_path_list.append(file_path)
            
        if len(image_path_list) < total_size:
            total_size = len(image_path_list)
            
        data_list = shuffle(image_path_list)  
        
        _train_size = int(total_size*(1-test_size))
        _test_size = int(total_size*(test_size)/2)
        _val_size = int(total_size*(test_size)/2)
        
        train_list = data_list[0:_train_size]
        test_list = data_list[_train_size:_train_size+_test_size]
        val_list = data_list[_train_size+_test_size:_train_size+_test_size+_val_size]
        
        print("train:%d test:%d val:%d"%(len(train_list), len(test_list), len(val_list)))

        path_list = [self.train_file_path, self.val_file_path, self.test_file_path]
        for pa in path_list:
            try:
                shutil.rmtree(pa)
            except OSError as e:
                print(e)
            else:
                print("The %s directory is deleted successfully"%(pa))
            os.makedirs('%s' % (pa), exist_ok=True)
            
        self.delete_ipynb_checkpoints()
        
        
        # Muti-processing
        print('### Creating Train/Val/Test Datasets For Pix2Pix ###')  
        pool_size = mp.cpu_count()
        print('pool size:%d'%(5))
        pool = Pool(processes=5) # Pool() 不放參數則默認使用電腦核的數量   
        pool.map(self.run_copy_train_file, train_list)
        pool.map(self.run_copy_val_file, val_list)
        pool.map(self.run_copy_test_file, test_list)
        pool.close()  
        pool.join()     
        print('=== Finishied %fs ==='%(time.time() - startTime))  
        
#         print('### Creating Train Datasets For Pix2Pix ###')   
#         for __path in train_list:      
#             basename = os.path.basename(__path)
#             save_path = '/'.join((self.train_file_path, basename))
#             shutil.copy(__path, save_path)
            
            
#         print('### Creating Val Datasets For Pix2Pix ###')   
#         for __path in val_list:      
#             basename = os.path.basename(__path)
#             save_path = '/'.join((self.val_file_path, basename))
#             shutil.copy(__path, save_path)
            
        
#         print('### Creating Test Datasets For Pix2Pix ###')   
#         for __path in test_list:      
#             basename = os.path.basename(__path)
#             save_path = '/'.join((self.test_file_path, basename))
#             shutil.copy(__path, save_path)
            
#         print('Finishied %fs'%(time.time() - startTime))  
            
#         data_list = pd.DataFrame({'path': image_path_list})

#         # 將資料清單隨機打亂
#         rand_seed = int(time())
#         data_list = shuffle(data_list, random_state=rand_seed)    
#         print(data_list.head(10))

#         #建立 X_train, X_test, X_valid 資料表
#         X = pd.DataFrame(data_list['path'], columns=['path'])
#         y = pd.DataFrame(data_list['path'], columns=['path'])
#         X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size)
#         X_train, X_valid, y_train, y_valid = train_test_split(X_train, y_train, test_size=test_size)

#         print(X_train.shape, X_valid.shape, X_test.shape)
        
#         import random
#         group_of_items = {1, 2, 3, 4}               # a sequence or set will work here.
#         num_to_select = 2                           # set the number to select here.
#         list_of_random_items = random.sample(group_of_items, num_to_select)
                
#         self.train_dir = 'train'
#         self.val_dir = 'val'
#         self.test_dir = 'test'
        
#         path_list = [self.train_file_path, self.val_file_path, self.test_file_path]
#         for pa in path_list:
#             try:
#                 shutil.rmtree(pa)
#             except OSError as e:
#                 print(e)
#             else:
#                 print("The %s directory is deleted successfully"%(pa))
#             os.makedirs('%s' % (pa), exist_ok=True)
            
#         self.delete_ipynb_checkpoints()
#         print('### Creating Train/Val/Test Datasets For Pix2Pix ###')    
        
#         for index, row in X_train.iterrows():      
#             basename = os.path.basename(row['path'])
#             save_path = '/'.join((self.train_file_path, basename))
#             shutil.copy(row['path'], save_path)
        
#         for index, row in X_valid.iterrows():
#             basename = os.path.basename(row['path'])
#             save_path = '/'.join((self.val_file_path, basename))
#             shutil.copy(row['path'], save_path)
        
#         for index, row in X_test.iterrows():
#             basename = os.path.basename(row['path'])
#             save_path = '/'.join((self.test_file_path, basename))
#             shutil.copy(row['path'], save_path)
        
    def init_datasets(self,output_root):
#         self.color_Do = (0,0,255)    # 紅色
#         self.color_Re = (0,97,255)   # 橙色
#         self.color_Mi = (0,255,255)　# 黃色
#         self.color_Fa = (0,255,0)　  # 綠色
#         self.color_Sol = (255,0,0)　 # 藍色
#         self.color_La = (84,46,8)　  # 靛色
#         self.color_Si = (240,32,160) # 紫色
        
#         self.color_C = (0,0,255)    # 紅色
#         self.color_D = (0,97,255)   # 橙色
#         self.color_E = (0,255,255)　# 黃色
#         self.color_F = (0,255,0)　  # 綠色
#         self.color_G = (255,0,0)　 # 藍色
#         self.color_A = (84,46,8)　  # 靛色
#         self.color_B = (240,32,160) # 紫色
        #################################################################################################################
        self.colorMap = ['red', 'green', 'blue', 'yellow', 'pink', 'orange', 'cyan', 'saddlebrown', 'maroon', 'tomato']
        self.original_dir = 'original'
        self.MIR_ST500 = 'MIR-ST500'
        self.CE200_sample = 'CE200_sample'
        self.CE200 = 'CE200'
        self.AIcup_testset_ok = 'AIcup_testset_ok'
        self.train_dir = 'train'
        self.val_dir = 'val'
        self.test_dir = 'test'
        self.features_dir = 'features'  
        self.output_root = output_root
        self.source_data_dir_path = './datasets/%s/%s'%(self.original_dir,self.CE200) 
        self.source_testdata_dir_path = './datasets/%s/%s'%(self.original_dir,self.AIcup_testset_ok)         
        self.feature_file_path = '/'.join((output_root, self.features_dir)) 
        self.train_file_path = '/'.join((output_root, self.train_dir))
        self.val_file_path = '/'.join((output_root, self.val_dir))
        self.test_file_path = '/'.join((output_root, self.test_dir))  
        #################################################################################################################
        os.makedirs(self.output_root, exist_ok=True)  
        os.makedirs(self.source_data_dir_path, exist_ok=True)  
        os.makedirs(self.source_testdata_dir_path, exist_ok=True)  
        os.makedirs(self.feature_file_path, exist_ok=True)  
        os.makedirs(self.train_file_path, exist_ok=True)  
        os.makedirs(self.val_file_path, exist_ok=True)  
        os.makedirs(self.test_file_path, exist_ok=True)  

    def parser_mp3datasets(self, datasetPath, toCsvName):
        datasetsPath = Path(datasetPath)

        # mp3 path
        mp3_path_list = []
        links_path = datasetsPath.glob('**/*.mp3')
        for link in links_path:
            mp3_path_list.append(link)
            
            
         # gen csv    
        data = pd.DataFrame()
        data['paths'] = mp3_path_list
        data.to_csv(os.path.join('./', toCsvName), index=False)        
        return mp3_path_list
        
        
    def parser_datasets2csv(self, datasetPath, toCsvName, isTrain=True ):
        datasetsPath = Path(datasetPath)

        # video_links
        links_path = datasetsPath.glob('*/*_link.txt')
        video_links = []
        for link in links_path:
            with open(link,'r', newline=None) as fp:
                for line in fp.readlines():
                    line = line.strip()
                    video_links.append(line)
                    break

        songs = []
        paths = []
        groundtruth = []
        features = []

        links_path = datasetsPath.glob('*/*_link.txt')
        for link in links_path:
            dirname  = os.path.dirname(link)
            basename = os.path.basename(dirname)
            paths.append(dirname)
            songs.append('%s.mp3'%(basename))
            ##################################################
            json_path = Path(dirname).glob('*.json')
            j_list=[]
            for j in json_path:
                __basename = os.path.basename(j)
                j_list.append(__basename)
            if j_list == []:
                j_list.append('None')
            features.append(str(j_list[0]))
            ###################################################
            groundtruth_path = Path(dirname).glob('*truth.txt')
            g_list=[]
            for g in groundtruth_path:
                __basename = os.path.basename(g)
                g_list.append(__basename)
            if g_list == []:
                g_list.append('None')
            groundtruth.append(str(g_list[0]))

        # gen csv    
        data = pd.DataFrame()
        data['paths'] = paths
        data['songs'] = songs
        data['features'] = features
        data['groundtruth'] = groundtruth
        data['video_links'] = video_links

        data.to_csv(os.path.join('./', toCsvName), index=False)
        print('=== %s Done ==='%('Train' if isTrain == True else 'Test'))
    
    def download_song2mp3(self, csvPath, start_index=0, end_index=-1):
        data = pd.read_csv(csvPath)        
        if end_index == -1:
            e_i = data.shape[0]
        s_i = start_index
        e_i = end_index
        for path,video_link,song in zip(data['paths'][s_i:e_i],data['video_links'][s_i:e_i],data['songs'][s_i:e_i]):
            print('=== %d ==='%(start_index))
            self.download_video('%s/%s'%(path,song.split(".")[0]), video_link)
            start_index += 1
    
    def download_video(self,title, video_url):
        ydl_opts = {
            'outtmpl': '{}.%(ext)s'.format(title),
            'format': 'bestaudio/best',
            'postprocessors': [{
                'key': 'FFmpegExtractAudio',
                'preferredcodec': 'mp3',
                'preferredquality': '192',
            }],
        }

        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            try:
                ydl.download([video_url])
            except ValueError:
                print("Oops!  That was no valid number.  Try again...")

        return {
            'audio': open('{}.mp3'.format(title), 'rb'),
            'title': title,
        } 
    
    
    
    def run_cut_full_image(self, song_path, show_path, h_flip=False):
#         dirname = os.path.dirname(song_path)
#         basename = os.path.basename(dirname)  
        number = os.path.splitext(os.path.basename(song_path))[0]
        count = 1

        songImg = cv2.imread(song_path)
        showImg = cv2.imread(show_path)
        w, h ,c = showImg.shape
               
        # check loop length must to be min (song_h, show_h)
        song_w, song_h ,song_c = songImg.shape 
        if song_h < h:
            w = song_w
            h = song_h
            c = song_c
            # return 0
        
        if self.shift:
            shift_window = self.time_steps / self.pixel_interval
        else:
            shift_window = 256
        window_width = 256

        for offset_i in (np.arange(0,window_width,shift_window)):  
            window_size = int((h-offset_i)/window_width) 
            for ind in range(window_size):
                start = 256 * (ind) + int(offset_i) 
                end = 256 * (ind + 1) + int(offset_i)
                diff = end - start
                newSongImg = songImg[:,start:end,:]
                newShowImg = showImg[:,start:end,:]
                save_pathA = '/'.join((self.feature_file_path, '{}_{}_A.png'.format(number,count)))
                save_pathB = '/'.join((self.feature_file_path, '{}_{}_B.png'.format(number,count)))
                # data = np.hstack((newSongImg,newShowImg)) # Domain A & B
                # cv2.imwrite(save_path, data)
                cv2.imwrite(save_pathA, newSongImg) # Domain A
                cv2.imwrite(save_pathB, newShowImg) # Domain B
                count = count + 1

                if h_flip:
                    h_flip_Song = cv2.flip(newSongImg, 1)
                    h_flip_Show = cv2.flip(newShowImg, 1)
                    save_pathA = '/'.join((self.feature_file_path, '{}_{}_A.png'.format(number,count)))
                    save_pathB = '/'.join((self.feature_file_path, '{}_{}_B.png'.format(number,count)))
                    # data = np.hstack((h_flip_Song,h_flip_Show)) # Domain A & B
                    # cv2.imwrite(save_path, data)
                    cv2.imwrite(save_pathA, h_flip_Song)
                    cv2.imwrite(save_pathB, h_flip_Show)
                    count = count + 1
                
        return count

    def cut_full_image(self, song_full_path, show_full_path):
        file_count = 1
        for song_path, show_path in (zip(song_full_path, show_full_path)):
#             dirname = os.path.dirname(song_path)
#             basename = os.path.basename(dirname)
            number = os.path.splitext(os.path.basename(song_path))[0]
            count = 1
            print('====== [{}/{}] ======'.format(file_count,len(song_full_path)))
            print('====== {}_{} ======'.format(number,count))
            print('SONG ===> {} '.format(song_path))
            print('NOTE ===> {} '.format(show_path))

            songImg = cv2.imread(song_path)
            showImg = cv2.imread(show_path)
            w, h ,c = showImg.shape

            # check loop length must to be min (song_h, show_h)
            song_w, song_h ,song_c = songImg.shape 
            if song_h < h:
                w = song_w
                h = song_h
                c = song_c
                return 0

            print(w, h ,c)
            if self.shift:
                shift_window = self.time_steps / self.pixel_interval
            else:
                shift_window = 256
            window_width = 256

            for offset_i in (np.arange(0,window_width,shift_window)):  
                window_size = int((h-offset_i)/window_width) 
#                 print('<<offset {} , window_size {}>>'.format(round(offset_i, 1), window_size))
                for ind in range(window_size):
                    start = 256 * (ind) + int(offset_i) 
                    end = 256 * (ind + 1) + int(offset_i)
                    diff = end - start
                    newSongImg = songImg[:,start:end,:]
                    newShowImg = showImg[:,start:end,:]
                    save_path = '/'.join((self.feature_file_path, '{}_{}.png'.format(number,count)))
                    data = np.hstack((newSongImg,newShowImg)) # Domain A & B
                    cv2.imwrite(save_path, data)                  
                    print(f'{count} -> {start}-{end} : {diff}')
                    count = count + 1
            file_count += 1

    def cut_features_image(self, song_full_path,fix_dirname='features'):
        self.delete_ipynb_checkpoints()
        import time as ts
        startTime = ts.time()
        file_count = 1
        for song_path in (song_full_path):
            dirname = os.path.dirname(song_path)
            output_features_path = '%s/%s/' % (dirname,fix_dirname)
            print('[GARY]',output_features_path)
            ###################################################################################################
            try:
                shutil.rmtree(output_features_path)
            except OSError as e:
                print(e)                
            else:
                print("The %s directory is deleted successfully"%(output_features_path))
            os.makedirs(output_features_path, exist_ok=True)  
            print('The %s directory is created successfully'%(output_features_path))
            ###################################################################################################
            number = os.path.basename(dirname)
            count = 1
            print('====== [{}/{}] ======'.format(file_count,len(song_full_path)))
            print('====== {}_{} ======'.format(number,count))
            print('SONG ===> {} '.format(song_path))
            songImg = cv2.imread(song_path)
            w, h ,c = songImg.shape
            print(w, h ,c)
 
            if self.shift:
                shift_window = self.time_steps / self.pixel_interval
            else:
                shift_window = 256
            window_width = 256

            for offset_i in (np.arange(0,window_width,shift_window)):  
                window_size = int((h-offset_i)/window_width) 
                print('<<offset {} , window_size {}>>'.format(round(offset_i, 1), window_size))
                for ind in range(window_size):
                    start = 256 * (ind) + int(offset_i) 
                    end = 256 * (ind + 1) + int(offset_i)
                    diff = end - start
                    newSongImg = songImg[:,start:end,:]
                    save_path = '/'.join((output_features_path, '{}_{}.png'.format(number,count))) 
                    cv2.imwrite(save_path, newSongImg)                  
                    print(f'{count} -> {start}-{end} : {diff}')
                    count = count + 1
            file_count += 1
        print('[cut_features_image] Done ... %.3fs'%(ts.time() - startTime))

    def cut_groundTruth_image(self, groundTruth_path,fix_dirname='groundTruth'):
        self.delete_ipynb_checkpoints()
        file_count = 1
        for song_path in (groundTruth_path):
            dirname = os.path.dirname(song_path)
            output_features_path = '%s/%s/' % (dirname,fix_dirname)
            print('[GARY]',output_features_path)
            ###################################################################################################
            try:
                shutil.rmtree(output_features_path)
            except OSError as e:
                print(e)                
            else:
                print("The %s directory is deleted successfully"%(output_features_path))
            os.makedirs(output_features_path, exist_ok=True)  
            print('The %s directory is created successfully'%(output_features_path))
            ###################################################################################################
            number = os.path.basename(dirname)
            count = 1
            print('====== [{}/{}] ======'.format(file_count,len(groundTruth_path)))
            print('====== {}_{} ======'.format(number,count))
            print('SONG ===> {} '.format(song_path))
            songImg = cv2.imread(song_path)
            w, h ,c = songImg.shape
            print(w, h ,c)
 
            if self.shift:
                shift_window = self.time_steps / self.pixel_interval
            else:
                shift_window = 256
            window_width = 256

            for offset_i in (np.arange(0,window_width,shift_window)):  
                window_size = int((h-offset_i)/window_width) 
                print('<<offset {} , window_size {}>>'.format(round(offset_i, 1), window_size))
                for ind in range(window_size):
                    start = 256 * (ind) + int(offset_i) 
                    end = 256 * (ind + 1) + int(offset_i)
                    diff = end - start
                    newSongImg = songImg[:,start:end,:]
                    save_path = '/'.join((output_features_path, '{}_{}.png'.format(number,count))) 
                    cv2.imwrite(save_path, newSongImg)                  
                    print(f'{count} -> {start}-{end} : {diff}')
                    count = count + 1
            file_count += 1
           
    def delete_ipynb_checkpoints(self):
        # delete all .ipynb_checkpoints dir
        for filename in Path(os.getcwd()).glob('**/*.ipynb_checkpoints'):
            try:
                shutil.rmtree(filename)
            except OSError as e:
                print(e)
            else: 
                print("The %s is deleted successfully" % (filename))

    def normalize(self,x, axis=0):
        return sklearn.preprocessing.minmax_scale(x, axis=axis)
    
    def drawWavefrom(self, y, sr, song_num):
        librosa.display.waveplot(y, sr=self.sample_rate, linewidth=0.01, alpha=0.5, x_axis='ms')
    
    def drawCentroids(self,y, sr, song_num):
        # 計算頻譜質心
        spectral_centroids = librosa.feature.spectral_centroid(y, sr=self.sample_rate)[0]
        frames = range(len(spectral_centroids))
        t = librosa.frames_to_time(frames)

        # 節奏 onset strength # Irene
        onset_env = librosa.onset.onset_strength(y=y, sr=self.sample_rate)
        plt.plot(t, 1+self.normalize(onset_env), color='g', linewidth=0.01, alpha=0.5)
            
        # Median aggregation, and custom mel options
        onset_env = librosa.onset.onset_strength(y=y, sr=self.sample_rate, aggregate=np.median, fmax=8000, n_mels=256)
        times = librosa.frames_to_time(np.arange(len(onset_env)), sr=self.sample_rate)
        plt.plot(times, onset_env / onset_env.max(), color='c', linewidth=0.01, alpha=0.5)
            

    def draw_pitch(self, pitch, ylim_min=0, ylim_max=600):
        # Extract selected pitch contour, and
        # replace unvoiced samples by NaN to not plot
        pitch_values = pitch.selected_array['frequency']        
        print(pitch_values.shape)
        pitch_values[pitch_values==0] = np.nan
        midi_number=librosa.hz_to_midi(pitch_values)
        midi_number=midi_number%12/12
#         midi_number[midi_number == np.nan] = 0
#         midi_number[midi_number == -np.inf] = 0
        
#         l = [x for x in midi_number if ~np.isnan(x)]
#         print(l)
        
        count = 0
        l = []
        for x in midi_number:
            if np.isnan(x):
                l.append(1.0)
            else:
                l.append(x)
            count += 1
#         print(l)
#         l = np.array(l)
        
#         midi_number = librosa.hz_to_midi(pitch_values) if pitch_values != np.nan else 0
#         print(pitch.xs())
        
#         midi_number[midi_number == -np.inf] = 0
#         midi_number[midi_number == np.nan] = 0
#         print(aaa)
#         midi_number[midi_number == -np.inf] = 0
        
#         print(librosa.hz_to_midi(pitch_values))
#         pitch_values[pitch_values<ylim_min] = np.nan
#         pitch_values[pitch_values>ylim_max] = np.nan
#         plt.plot(pitch.xs(), pitch_values, '-.',linewidth=1, markersize=1, color='orange')
#         plt.plot(pitch.xs(), pitch_values, '.', markersize=0.05, color='r')
#         plt.plot(pitch.xs(), pitch_values, 'o', markersize=0.01, linewidth=0.01, color='r')
#         plt.scatter(pitch.xs(),pitch_values, s=.01, color='r')
#         plt.plot(pitch.xs(), pitch_values, 'o-',linewidth=.01, markersize=0.01, color='orange')
#         plt.grid(False)
#         plt.bar(pitch.xs(),pitch_values,linewidth=0.01, align='edge', width = 0.01, edgecolor='b', color='b', alpha=0.5, cmap='rainbow')       
        plt.bar(pitch.xs(), pitch_values, linewidth=0.01, align='edge', width = 0.01,  alpha=.5,
                edgecolor=self.midiColor(l), color=self.midiColor(l))
     
#         plt.plot(pitch.xs(),pitch_values, '-.',linewidth=.5, alpha=0.5, color=self.midiColor(l) )
        plt.ylim(ylim_min, ylim_max)
#         plt.ylabel("fundamental frequency [Hz]")

        plt.xticks([])
        plt.yticks([])
 
        
            
    def draw_spectrogram(self,spectrogram,dynamic_range=70):
        X, Y = spectrogram.x_grid(), spectrogram.y_grid()
        sg_db = 10 * np.log10(spectrogram.values)
#         plt.pcolormesh(X, Y, sg_db, vmin=sg_db.max() - dynamic_range, cmap='afmhot')
        plt.pcolormesh(X, Y, sg_db, vmin=sg_db.max() - dynamic_range)

        plt.ylim([spectrogram.ymin, spectrogram.ymax])
#         plt.xlabel("time [s]")
#         plt.ylabel("frequency [Hz]")
        plt.xticks([])
        plt.yticks([])

    def draw_intensity(self, intensity, db_min=0, db_max=100):
#         plt.plot(intensity.xs(), intensity.values.T, linewidth=3, color='w')
#         plt.plot(intensity.xs(), intensity.values.T, linewidth=1)
#         plt.plot(intensity.xs(), intensity.values.T, linewidth=1, color='w')
        plt.plot(intensity.xs(), intensity.values.T, linewidth=.5, color='r', alpha=0.5)
        plt.grid(False)
        plt.ylim(db_min,db_max)
#         plt.ylabel("intensity [dB]")
        plt.xticks([])
        plt.yticks([])
            
    def drawMFCC(self, y, sr):
        mfccs = librosa.feature.mfcc(y=y, sr=self.sample_rate, hop_length=self.hop_length, n_mfcc=40)
#         _mfccs = mfccs.astype('float64')
        _mfccs = np.array(mfccs).astype('float64')
        _mfccs = sklearn.preprocessing.scale(_mfccs, axis=1)
#         librosa.display.specshow(mfccs, sr=self.sample_rate, x_axis='time',y_axis='linear',  linewidth=0.01)  
#         librosa.display.specshow(mfccs, sr=fs, x_axis='ms',  y_axis='hz', linewidth=0.01)
        librosa.display.specshow(_mfccs, sr=self.sample_rate, x_axis='ms',  y_axis='mel', linewidth=0.01)
        plt.ylim([0, 8192])
        plt.xticks([])
        plt.yticks([])
        return mfccs
#.............................................................................................................
    def drawMFCC_delta(self, mfcc, sr):
#         _mfccs = mfcc.astype('float32')
        _mfccs = np.array(mfcc).astype('float64')
        _mfccs = sklearn.preprocessing.scale(_mfccs, axis=1)
        mfcc_delta = librosa.feature.delta(_mfccs)
  
        librosa.display.specshow(mfcc_delta, sr=self.sample_rate, x_axis='ms',  y_axis='mel', linewidth=0.01)
        plt.ylim([0, 8192])
        plt.xticks([])
        plt.yticks([])
        
    def drawSTFT(self, y, sr):
        # STFT 語音頻譜分析圖
        D = librosa.amplitude_to_db(abs(librosa.stft(y)), ref=np.max)
#         librosa.display.specshow(D, x_axis='time', y_axis='linear',sr=self.sample_rate,  cmap='coolwarm', linewidth=0.01)
        librosa.display.specshow(D, x_axis='ms', y_axis='log',sr=self.sample_rate, linewidth=0.01, fmin=0.0, fmax=5000.0)
        plt.xticks([])
        plt.yticks([])
    def drawChroma_stft(self,y, sr):
        chromagram=librosa.feature.chroma_stft(y=y, sr=self.sample_rate, n_fft=512, hop_length=self.hop_length)
        librosa.display.specshow(chromagram, x_axis='ms', y_axis='chroma', sr=self.sample_rate, hop_length=self.hop_length, cmap='coolwarm', linewidth=0.01)
        plt.xticks([])
        plt.yticks([])   
    def drawTonnetz(self,y,sr):
        chromagram = librosa.feature.tonnetz(y=y, sr=self.sample_rate)
        librosa.display.specshow(chromagram, x_axis='ms', y_axis='chroma', sr=self.sample_rate, hop_length=self.hop_length, cmap='coolwarm', linewidth=0.01)
    def drawChroma_cqt(self,y, sr):
        # chromagram = librosa.feature.chroma_cqt(y=y, sr=self.sample_rate, bins_per_octave=12*3)
        chromagram = librosa.feature.chroma_cqt(y=y, sr=self.sample_rate)
        librosa.display.specshow(chromagram, x_axis='ms', y_axis='chroma', hop_length=self.hop_length, sr=self.sample_rate, linewidth=0.01, cmap='coolwarm',)
        plt.xticks([])
        plt.yticks([])        
    def drawChroma_cens(self,y, sr):
        # chromagram = librosa.feature.chroma_cens(y, sr=self.sample_rate, hop_length=self.hop_length, n_chroma=12*2)
        chromagram = librosa.feature.chroma_cens(y, sr=self.sample_rate, hop_length=self.hop_length)
        librosa.display.specshow(chromagram, x_axis='ms', y_axis='chroma', sr=self.sample_rate, hop_length=self.hop_length, cmap='coolwarm')   
        plt.xticks([])
        plt.yticks([])     
    def drawChroma_harmonic_cens(self,y, sr):
         # 計算 CQT+Chroma                
        y_harm = librosa.effects.harmonic(y=y, margin=8)
        chroma_os_harm = librosa.feature.chroma_cqt(y=y_harm, sr=self.sample_rate, bins_per_octave=12*3)
        chromagram = librosa.feature.chroma_cens(y,sr=self.sample_rate,C=chroma_os_harm, bins_per_octave=12*3, n_chroma=12)
        librosa.display.specshow(chromagram, x_axis='ms', y_axis='chroma',sr=self.sample_rate, linewidth=0.01)        
    def drawChroma_smooth(self,y, sr):
#         chroma_orig = librosa.feature.chroma_cqt(y=y, sr=self.sample_rate)
#         C = np.abs(librosa.cqt(y=y, sr=self.sample_rate, bins_per_octave=12*3, n_bins=7*12*3))
#         librosa.display.specshow(librosa.amplitude_to_db(C, ref=np.max)[idx], y_axis='cqt_note', bins_per_octave=12*3)
  
        y_harm = librosa.effects.harmonic(y=y, margin=8)
        chroma_os_harm = librosa.feature.chroma_cqt(y=y_harm, sr=self.sample_rate, bins_per_octave=12*3)
        chroma_filter = np.minimum(chroma_os_harm,
                           librosa.decompose.nn_filter(chroma_os_harm,
                                                       aggregate=np.median,
                                                       metric='cosine'))
        chroma_smooth = scipy.ndimage.median_filter(chroma_filter, size=(1, 9))
        librosa.display.specshow(chroma_smooth, x_axis='ms', y_axis='chroma',sr=self.sample_rate, linewidth=0.01)
        
    def run_Creating_Datasets_ALLINONE(self, song_path, offset=0.0, duration=None, prefix_name=None,old_ver=False): 
        start_time = datetime.datetime.now()
        dirname = os.path.dirname(song_path)
        basename = os.path.basename(song_path)
        songname = os.path.splitext(basename)[0]
        if prefix_name is None:
            save_path = '%s/%s_feature_full.png'%(dirname,songname)
        else:
            save_path = '%s/%s_%s_feature_full.png'%(dirname,songname,prefix_name)
        ax_total = 2 if old_ver else 5
        ax_num = 1
        w = 256
        h = 256
        y, sr = librosa.load(song_path,sr=self.sample_rate, offset=offset, duration=duration)
        # snd = parselmouth.Sound(song_path)
        total_sec = librosa.get_duration(y=y, sr=self.sample_rate)
        w_ratio = int(math.ceil(total_sec / self.time_interval))
        w = w * w_ratio
        print('[song_path:{}] === width : {} , total_sec: {}, wanted_sec: {}==='.format(song_path, w, total_sec, w_ratio*self.time_interval))
        num_pad = round(w_ratio * self.time_interval * self.sample_rate - y.shape[0])
        y = np.pad(y, (0, num_pad), 'constant', constant_values=(0, 0))            
        plt.style.use('dark_background')
        fig = plt.figure(0)

        # AX [1]
        ax1 = plt.subplot(ax_total,1,ax_num)
        ax1.axis('off')
        ax_num += 1            
        self.drawMFCC(y=y, sr=self.sample_rate)
     
        # AX [2]
        plt.xticks([])
        plt.yticks([])
        ax2 = plt.subplot(ax_total,1,ax_num, sharex=ax1)
        ax2.axis('off')
        ax_num += 1
        self.drawChroma_cens(y=y, sr=self.sample_rate)

        if old_ver == False:
            # AX [3]
            plt.xticks([])
            plt.yticks([])
            ax3 = plt.subplot(ax_total,1,ax_num, sharex=ax1)
            ax3.axis('off')
            ax_num += 1
            self.drawChroma_cqt(y=y, sr=self.sample_rate)

            # AX [4]
            plt.xticks([])
            plt.yticks([])
            ax4 = plt.subplot(ax_total,1,ax_num, sharex=ax1)
            ax4.axis('off')
            ax_num += 1
            self.drawChroma_stft(y=y, sr=self.sample_rate)

            # AX [5]
            plt.xticks([])
            plt.yticks([])
            ax5 = plt.subplot(ax_total,1,ax_num, sharex=ax1)
            ax5.axis('off')
            ax_num += 1
            self.drawTonnetz(y=y, sr=self.sample_rate)
        
        ##################################################################################
        plt.axis('off')
        fig.set_size_inches((w/100.0)/3,(h/100.0)/3) #dpi = 300, output = 256*256 pixels
        plt.subplots_adjust(top = 1, bottom = 0, right = 1, left = 0, hspace = 0, wspace = 0)
        plt.margins(0,0)
        corp_song_name_path = '%s_crop_song.png'%(songname)  
        fig.savefig(corp_song_name_path, format='png', transparent=True, dpi=300, pad_inches = 0)
        inputs = cv2.imread(corp_song_name_path)
        data = inputs        
        
        try:
            os.remove(save_path)
        except OSError as e:
            # print(e)
            pass
        else: 
            pass
            
        try:
            os.remove(corp_song_name_path)
        except OSError as e:
            # print(e)
            pass
        else: 
            pass
                        
        cv2.imwrite(save_path, data)
        plt.close(fig)
        elapsed_time = datetime.datetime.now() - start_time

        if self.verbose:
            print('########################## song_path:%s #############################' % (song_path))
            print(' width: {} , total_sec: {}, wanted_sec: {}'.format(w, total_sec, w_ratio*self.time_interval))
            print(' elapsed_time: %s '%(elapsed_time))
    
    def getnpz_onset_beat_plp(self,y,sr,dirname,songname):
        midi_note_onset = []
        midi_note_plp = []
        midi_note_beats = []
    
        y, index = librosa.effects.trim(y)
        
        # Onset 
        onset_frames = librosa.onset.onset_detect(y=y, sr=self.sample_rate)
        onset_times = librosa.frames_to_time(onset_frames, sr=self.sample_rate)
        
        hop_length = 512
        window = 'hann'
        bins_per_octave = 12
        n_octaves = 7
        n_bins = bins_per_octave * n_octaves
        cqt_amplitude = np.abs(librosa.cqt(y, sr=self.sample_rate, hop_length=hop_length, fmin=librosa.note_to_hz('C1'), n_bins=n_bins,
                                        bins_per_octave=bins_per_octave, window=window))
        notes = librosa.hz_to_note(librosa.cqt_frequencies(n_bins=n_bins, fmin=librosa.note_to_hz('C1'), bins_per_octave=bins_per_octave))

        end_frames = onset_frames[1:]
        end_frames = np.append(end_frames, cqt_amplitude.shape[1])
        for onset_frame, end_frame in zip(onset_frames, end_frames):
            max_indices = np.argmax(cqt_amplitude[:, onset_frame:end_frame], axis=0)
            max_index, count = stats.mode(max_indices)
            onset_time = librosa.core.frames_to_time(onset_frame, sr=self.sample_rate, hop_length=hop_length)
            end_time = librosa.core.frames_to_time(end_frame, sr=self.sample_rate, hop_length=hop_length)
            
            midi = librosa.note_to_midi(notes[max_index[0]])
            midi_note_onset.append([round(onset_time,3), round(end_time,3), round(midi,3)])
    #         print(f't: {onset_time:.2f}-{end_time:.2f}[s], note: {notes[max_index[0]]} midi: {midi}')

        # Beats and PLP
        # win_length = 384
        onset_env = librosa.onset.onset_strength(y=y, sr=self.sample_rate, 
                                                hop_length=hop_length,) 
    #                                              aggregate=np.median)
        # pulse = librosa.beat.plp(onset_envelope=onset_env, sr=self.sample_rate, win_length=win_length)
        self.tempo = librosa.beat.tempo(onset_envelope=onset_env)
        # tempo, beats = librosa.beat.beat_track(onset_envelope=onset_env)
        print('tempo:',self.tempo)

        # beats_times = librosa.frames_to_time(beats, sr=self.sample_rate)
        # beats_plp = np.flatnonzero(librosa.util.localmax(pulse))
        # times = librosa.times_like(pulse, sr=self.sample_rate)

        # for i,time in enumerate(times[beats_plp]):
        #     if i < len(times[beats_plp])-1:
        #         midi_note_plp.append([times[beats_plp][i],times[beats_plp][i+1],60])
                
        # for i,time in enumerate(beats_times):
        #     if i < len(beats_times)-1:
        #         midi_note_beats.append([beats_times[i],beats_times[i+1],60])
        
        # np.savez('%s/%s_%s.npz'%(dirname,songname,'beats'), midi_note=midi_note_beats, tempo=tempo)
        # np.savez('%s/%s_%s.npz'%(dirname,songname,'plp'), midi_note=midi_note_plp, tempo=tempo)
        # np.savez('%s/%s_%s.npz'%(dirname,songname,'onset'), midi_note=midi_note_onset)
        # print(tempo,tempo1,len(beats_times),len(beats_plp),len(midi_note_onset))
    def Creating_feature_full_encoder(self, files_MP3, start, end, offset=0.0, duration=None, prefix_name=None):   
        for song_path in files_MP3[start:end]:
            w = 256
            h = 256
            start_time = datetime.datetime.now()
            dirname = os.path.dirname(song_path)
            basename = os.path.basename(song_path)
            songname = os.path.splitext(basename)[0]
            if prefix_name is None:
                save_path = '%s/%s_feature_full.png'%(dirname,songname)
            else:
                save_path = '%s/%s_%s_feature_full.png'%(dirname,songname,prefix_name)

            y, sr = librosa.load(song_path,sr=self.sample_rate, offset=offset, duration=duration)
            # total_sec = librosa.get_duration(y=y, sr=self.sample_rate)
            # w = int(math.ceil(total_sec / self.pixel_interval))
            w = int(math.ceil(len(y) / self.hop_length))
            # w = w * w_ratio
            print('[{}] === width : {} , total_sec: {} ==='.format(song_path, w, w*self.pixel_interval))

            feature_dict = dict()
            # MFCC 
            mfcc = librosa.feature.mfcc(y=y, sr=self.sample_rate, hop_length=self.hop_length, n_mfcc=40)
            mfcc = np.array(mfcc).astype('float32')
            feature_dict['mfcc'] = sklearn.preprocessing.scale(mfcc, axis=1)
            
            # chroma_stft, chroma_cens, chroma_cq
            feature_dict['chroma_stft'] = librosa.feature.chroma_stft(y=y, sr=self.sample_rate, n_fft=512, hop_length=self.hop_length)
            feature_dict['chroma_cens'] = librosa.feature.chroma_cens(y=y, sr=self.sample_rate, hop_length=self.hop_length)
            feature_dict['chroma_cqt'] = librosa.feature.chroma_cqt(y=y, sr=self.sample_rate, hop_length=self.hop_length)

            # rms
            feature_dict['rms'] = librosa.feature.rms(y=y, frame_length=512, hop_length=self.hop_length)

            # spectral_centroid
            feature_dict['spectral_centroid'] = librosa.feature.spectral_centroid(y=y, sr=self.sample_rate, n_fft=512, hop_length=self.hop_length)

            feature_dict['spectral_bandwidth'] = librosa.feature.spectral_bandwidth(y=y, sr=self.sample_rate, n_fft=512, hop_length=self.hop_length)

            feature_dict['spectral_contrast'] = librosa.feature.spectral_contrast(y=y, sr=self.sample_rate, n_fft=512, hop_length=self.hop_length)

            feature_dict['spectral_flatness'] = librosa.feature.spectral_flatness(y=y, n_fft=512, hop_length=self.hop_length)

            feature_dict['spectral_rolloff'] = librosa.feature.spectral_rolloff(y=y, sr=self.sample_rate, n_fft=512, hop_length=self.hop_length)

            feature_dict['poly_features'] = librosa.feature.poly_features(y=y, sr=self.sample_rate, n_fft=512, hop_length=self.hop_length)

            feature_dict['tonnetz'] = librosa.feature.tonnetz(y=y, sr=self.sample_rate)

            feature_dict['zero_crossing_rate'] = librosa.feature.zero_crossing_rate(y=y, frame_length=512, hop_length=self.hop_length)

            ###############################
            # 產出 Chord Encode Image
            ###############################
            feature_full_image = np.zeros((256,w,3), np.uint8)
            for x in range(w):
                y = 0
                for key in feature_dict:
                    for i in range(len(feature_dict[key])):
                        code = BitArray(float=feature_dict[key][i][x], length=32)
                        b = int(code.bin[0:8], 2)
                        g = int(code.bin[8:16], 2)
                        r = int(code.bin[16:24], 2)
                        cv2.line(feature_full_image, (x, y), (x, y), (b,g,r), 1) 
                        b = int(code.bin[24:32], 2)
                        g = int(code.bin[24:32], 2)
                        r = int(code.bin[24:32], 2)
                        cv2.line(feature_full_image, (x, y+1), (x, y+1), (b,g,r), 1)                        
                        y+=2
                x+=1
            try:
                os.remove(save_path)
            except OSError as e:
                pass
            else: 
                pass
                
            cv2.imwrite(save_path, feature_full_image)
            elapsed_time = datetime.datetime.now() - start_time
            if self.verbose:
                print('########################## total time:%s #############################' % (elapsed_time))
#################################################################################################################
#################################################################################################################            
#################################################################################################################
    def run_Creating_feature_full_encoder(self, song_path, offset=0.0, duration=None, prefix_name=None): 
        w = 256
        h = 256
        start_time = datetime.datetime.now()
        dirname = os.path.dirname(song_path)
        basename = os.path.basename(song_path)
        songname = os.path.splitext(basename)[0]
        if prefix_name is None:
            save_path = '%s/%s_feature_full.png'%(dirname,songname)
        else:
            save_path = '%s/%s_%s_feature_full.png'%(dirname,songname,prefix_name)

        y, sr = librosa.load(song_path,sr=self.sample_rate, offset=offset, duration=duration)
        # total_sec = librosa.get_duration(y=y, sr=self.sample_rate)
        # w = int(math.ceil(total_sec / self.pixel_interval))
        w = int(math.ceil(len(y) / self.hop_length))
        # w = w * w_ratio
        print('[{}] === width : {} , total_sec: {} ==='.format(song_path, w, w*self.pixel_interval))

        feature_dict = dict()
        # MFCC 
        mfcc = librosa.feature.mfcc(y=y, sr=self.sample_rate, hop_length=self.hop_length, n_mfcc=40)
        mfcc = np.array(mfcc).astype('float32')
        feature_dict['mfcc'] = sklearn.preprocessing.scale(mfcc, axis=1)
        
        # chroma_stft, chroma_cens, chroma_cq
        feature_dict['chroma_stft'] = librosa.feature.chroma_stft(y=y, sr=self.sample_rate, n_fft=512, hop_length=self.hop_length)
        feature_dict['chroma_cens'] = librosa.feature.chroma_cens(y=y, sr=self.sample_rate, hop_length=self.hop_length)
        feature_dict['chroma_cqt'] = librosa.feature.chroma_cqt(y=y, sr=self.sample_rate, hop_length=self.hop_length)

        # rms
        feature_dict['rms'] = librosa.feature.rms(y=y, frame_length=512, hop_length=self.hop_length)

        # spectral_centroid
        feature_dict['spectral_centroid'] = librosa.feature.spectral_centroid(y=y, sr=self.sample_rate, n_fft=512, hop_length=self.hop_length)

        feature_dict['spectral_bandwidth'] = librosa.feature.spectral_bandwidth(y=y, sr=self.sample_rate, n_fft=512, hop_length=self.hop_length)

        feature_dict['spectral_contrast'] = librosa.feature.spectral_contrast(y=y, sr=self.sample_rate, n_fft=512, hop_length=self.hop_length)

        feature_dict['spectral_flatness'] = librosa.feature.spectral_flatness(y=y, n_fft=512, hop_length=self.hop_length)

        feature_dict['spectral_rolloff'] = librosa.feature.spectral_rolloff(y=y, sr=self.sample_rate, n_fft=512, hop_length=self.hop_length)

        feature_dict['poly_features'] = librosa.feature.poly_features(y=y, sr=self.sample_rate, n_fft=512, hop_length=self.hop_length)

        feature_dict['tonnetz'] = librosa.feature.tonnetz(y=y, sr=self.sample_rate)

        feature_dict['zero_crossing_rate'] = librosa.feature.zero_crossing_rate(y=y, frame_length=512, hop_length=self.hop_length)

        ###############################
        # 產出 Chord Encode Image
        ###############################
        feature_full_image = np.zeros((256,w,3), np.uint8)
        for x in range(w):
            y = 0
            for key in feature_dict:
                for i in range(len(feature_dict[key])):
                    code = BitArray(float=feature_dict[key][i][x], length=32)
                    b = int(code.bin[0:8], 2)
                    g = int(code.bin[8:16], 2)
                    r = int(code.bin[16:24], 2)
                    cv2.line(feature_full_image, (x, y), (x, y), (b,g,r), 1) 
                    b = int(code.bin[24:32], 2)
                    g = int(code.bin[24:32], 2)
                    r = int(code.bin[24:32], 2)
                    cv2.line(feature_full_image, (x, y+1), (x, y+1), (b,g,r), 1)                        
                    y+=2
            x+=1
        try:
            os.remove(save_path)
        except OSError as e:
            pass
        else: 
            pass
            
        cv2.imwrite(save_path, feature_full_image)
        elapsed_time = datetime.datetime.now() - start_time
        if self.verbose:
            print('########################## total time:%s #############################' % (elapsed_time))
    def Creating_Datasets_ALLINONE(self, files_MP3, start, end, offset=0.0, duration=None, prefix_name=None,old_ver=False):   
        song_num = 1
        # ax_total = 5
        ax_total = 2 if old_ver else 5
        for song_path in files_MP3[start:end]:
            ax_num = 1
            w = 256
            h = 256
            start_time = datetime.datetime.now()

            dirname = os.path.dirname(song_path)
            basename = os.path.basename(song_path)
            songname = os.path.splitext(basename)[0]
            if prefix_name is None:
                save_path = '%s/%s_feature_full.png'%(dirname,songname)
            else:
                save_path = '%s/%s_%s_feature_full.png'%(dirname,songname,prefix_name)

            y, sr = librosa.load(song_path,sr=self.sample_rate, offset=offset, duration=duration)
            # self.getnpz_onset_beat_plp(y, sr, dirname='./midi', songname=songname)
            # snd = parselmouth.Sound(song_path)
            total_sec = librosa.get_duration(y=y, sr=self.sample_rate)
            w_ratio = int(math.ceil(total_sec / self.time_interval))
            w = w * w_ratio
            if self.verbose:
                print('=== width : {} , total_sec: {}, wanted_sec: {} ==='.format(w, total_sec, w_ratio*self.time_interval), song_path)
            num_pad = round(w_ratio * self.time_interval * self.sample_rate - y.shape[0])
            y = np.pad(y, (0, num_pad), 'constant', constant_values=(0, 0))            
            plt.style.use('dark_background')
            fig = plt.figure(0)
            
            # AX [1]
            ax1 = plt.subplot(ax_total,1,ax_num)
            ax1.axis('off')
            ax_num += 1            
            self.drawMFCC(y=y, sr=self.sample_rate)
        
            # AX [2]
            plt.xticks([])
            plt.yticks([])
            ax2 = plt.subplot(ax_total,1,ax_num, sharex=ax1)
            ax2.axis('off')
            ax_num += 1
            self.drawChroma_cens(y=y, sr=self.sample_rate)
            
            if old_ver == False:
                # AX [3]
                plt.xticks([])
                plt.yticks([])
                ax3 = plt.subplot(ax_total,1,ax_num, sharex=ax1)
                ax3.axis('off')
                ax_num += 1
                self.drawChroma_cqt(y=y, sr=self.sample_rate)

                # AX [4]
                plt.xticks([])
                plt.yticks([])
                ax4 = plt.subplot(ax_total,1,ax_num, sharex=ax1)
                ax4.axis('off')
                ax_num += 1
                self.drawChroma_stft(y=y, sr=self.sample_rate)

                # AX [5]
                plt.xticks([])
                plt.yticks([])
                ax5 = plt.subplot(ax_total,1,ax_num, sharex=ax1)
                ax5.axis('off')
                ax_num += 1
                self.drawTonnetz(y=y, sr=self.sample_rate)
            
            ##################################################################################
            plt.axis('off')
            fig.set_size_inches((w/100.0)/3,(h/100.0)/3) #dpi = 300, output = 256*256 pixels
            plt.subplots_adjust(top = 1, bottom = 0, right = 1, left = 0, hspace = 0, wspace = 0)
            plt.margins(0,0)
            corp_song_name_path = '%s_crop_song.png'%(songname) 
            fig.savefig(corp_song_name_path, format='png', transparent=True, dpi=300, pad_inches = 0)
            inputs = cv2.imread(corp_song_name_path)
            data = inputs
            # dirname = os.path.dirname(song_path)
            # basename = os.path.basename(song_path)
            # songname = os.path.splitext(basename)[0]
            
            # if prefix_name is None:
            #     save_path = '%s/%s_feature_full.png'%(dirname,songname)
            # else:
            #     save_path = '%s/%s_%s_feature_full.png'%(dirname,songname,prefix_name)
            
            # try:
            #     os.remove(save_path)
            # except OSError as e:
            #     if self.verbose:
            #         print(e)
            # else: 
            #     if self.verbose:
            #         print("The %s is deleted successfully" % (save_path))
            #     pass

            try:
                os.remove(save_path)
            except OSError as e:
                # print(e)
                pass
            else: 
                pass
                
            try:
                os.remove(corp_song_name_path)
            except OSError as e:
                # print(e)
                pass
            else: 
                pass

            cv2.imwrite(save_path, data)
            plt.close(fig)
            song_num += 1
            elapsed_time = datetime.datetime.now() - start_time
            if self.verbose:
                print('########################## total time:%s #############################' % (elapsed_time))
    
    def song2img4all(self, csvFile):
        data = pd.read_csv(csvFile)
        count = 1
        files_MP3 = []
        files_note = []
        for _path, _groundtruth, _songs in zip(data['paths'],data['groundtruth'],data['songs']):
            mp3_path = os.path.join(_path, _songs)
            groundtruth_path = os.path.join(_path, _groundtruth)
            basename = os.path.basename(groundtruth_path)
            imgname = '%s.png'%(os.path.splitext(basename)[0])
            if path.exists(mp3_path) == True:
                files_MP3.append(mp3_path)
        self.Creating_Datasets_ALLINONE(files_MP3, 0, len(files_MP3))
        
    def song2img4one(self, mp3_path, offset=0.0, duration=None, prefix_name=None, rule='Normal',old_ver=False):
        import time as ts
        startTime = ts.time()
        if rule == 'Normal':
            self.Creating_Datasets_ALLINONE([mp3_path], 0, 1, offset=offset, duration=duration, prefix_name=prefix_name,old_ver=old_ver)
        else:
            self.Creating_feature_full_encoder([mp3_path], 0, 1, offset=offset, duration=duration, prefix_name=prefix_name)
        print('[song2img4one] Done ... %.3fs'%(ts.time() - startTime))
                
    def run(self, command):
        if self.verbose:
            print(command)
        exit_status = os.system(command)
        if exit_status > 0:
            exit(1)

    def predict(self, epoch='latest', song_id=1, dataroot='./datasets/original/AIcup_testset_ok', CLASS='singing_transcription', MODEL='pix2pix', copy_model=True, NL='--nl relu'):
        startTime = time.time()
        ##############################################
        NET_SIZE = 64
        NORM='batch'
        # EPOCH = '--epoch %s'%(epoch)
        # if copy_model:
        #     EPOCH = epoch
        ##############################################
        NEF='%d'%(NET_SIZE)
        NGF='%d'%(NET_SIZE)
        NDF='%d'%(NET_SIZE)
        G_MODEL_NAME = '%s_net_G.pth'%(epoch)
        D_MODEL_NAME = '%s_net_D.pth'%(epoch)
        EPOCH = '--epoch %s'%(epoch)
        print(G_MODEL_NAME,D_MODEL_NAME)
        ##############################################
        CMD='./predict_pitch.py' 
        MODEL=MODEL
        DIRECTION='AtoB' # from domain A to domain B
        LOAD_SIZE=256 # scale images to this size
        CROP_SIZE=256 # then crop to this size
        INPUT_NC=3  # number of channels in the input image
        NO_FLIP='--no_flip'
        DATAROOT='%s/%d/features'%(dataroot,song_id)
        print(DATAROOT)
        CHECKPOINTS_DIR='./pretrained_models/%s/'%(CLASS)
        # PHASE='--phase predict'
        PHASE='--phase predict_test'
        CPU='--gpu_ids -1'
        GPU_ID=0   # gpu id
        NUM_TEST=1000 # number of input images duirng test
        # number of samples per input images
        NUM_SAMPLES='--n_samples 1' 
        # USE_DROPOUT='--use_dropout'
        NAME='%s_%s'%(CLASS,MODEL)
        RESULTS_DIR='./results/%s/'%(NAME)
        # RESULTS_DIR='./results/'
        NET_G='--netG unet_256'
        DATASET_MODE='--dataset_mode single'#'aligned'
        ##############################################
        os.environ['CMD']=str(CMD)
        os.environ['NL']=str(NL)
        os.environ['MODEL']=str(MODEL)
        os.environ['CLASS']=str(CLASS)
        os.environ['DIRECTION']=str(DIRECTION)
        os.environ['LOAD_SIZE']=str(LOAD_SIZE)
        os.environ['CROP_SIZE']=str(CROP_SIZE)
        os.environ['INPUT_NC']=str(INPUT_NC)
        os.environ['NO_FLIP']=str(NO_FLIP)
        os.environ['DATAROOT']=str(DATAROOT)
        os.environ['CHECKPOINTS_DIR']=str(CHECKPOINTS_DIR)
        os.environ['PHASE']=str(PHASE)
        os.environ['CPU']=str(CPU)
        os.environ['GPU_ID']=str(GPU_ID)
        os.environ['NUM_TEST']=str(NUM_TEST)
        os.environ['NUM_SAMPLES']=str(NUM_SAMPLES)
        os.environ['NAME']=str(NAME)
        os.environ['RESULTS_DIR']=str(RESULTS_DIR)
        os.environ['NET_G']=str(NET_G)
        # os.environ['USE_DROPOUT']=str(USE_DROPOUT)
        os.environ['DATASET_MODE']=str(DATASET_MODE)
        os.environ['G_MODEL_NAME']=str(G_MODEL_NAME)
        os.environ['D_MODEL_NAME']=str(D_MODEL_NAME)
        os.environ['EPOCH']=str(EPOCH)

        os.environ['NEF']=str(NEF)
        os.environ['NGF']=str(NGF)
        os.environ['NDF']=str(NDF)
        ##############################################
#         self.original_dir = 'original'
#         self.MIR_ST500 = 'MIR-ST500'
#         self.AIcup_testset_ok = 'AIcup_testset_ok'
#         self.train_dir = 'train'
#         self.val_dir = 'val'
#         self.test_dir = 'test'
#         self.features_dir = 'features'
#         self.source_data_dir_path = './datasets/%s/%s'%(self.original_dir,self.MIR_ST500) 
#         self.source_testdata_dir_path = './datasets/%s/%s'%(self.original_dir,self.AIcup_testset_ok)       
#         self.feature_file_path = '/'.join((output_root, self.features_dir)) 
#         self.train_file_path = '/'.join((output_root, self.train_dir))
#         self.val_file_path = '/'.join((output_root, self.val_dir))
#         self.test_file_path = '/'.join((output_root, self.test_dir))
#         ##############################################
#         self.model_source_path = '../../BicycleGAN/checkpoints/%s/%s_%s/'%(CLASS,CLASS,MODEL)
#         self.pretrained_models_path = './pretrained_models/%s/%s/'
        ##############################################
        # if NET_SIZE == 64 and NORM == 'batch':
        #     self.run('cp -rf ./checkpoints/singing_transcription/singing_transcription_pix2pix/$G_MODEL_NAME ./pretrained_models/singing_transcription/singing_transcription_pix2pix/')
        #     self.run('cp -rf ./checkpoints/singing_transcription/singing_transcription_pix2pix/$D_MODEL_NAME ./pretrained_models/singing_transcription/singing_transcription_pix2pix/')            
        # elif NET_SIZE == 64 and NORM == 'instance':
        #     self.run('cp -rf ./checkpoints/singing_transcription_64_instance/singing_transcription_64_instance_pix2pix/$G_MODEL_NAME ./pretrained_models/singing_transcription/singing_transcription_pix2pix/')
        #     self.run('cp -rf ./checkpoints/singing_transcription_64_instance/singing_transcription_64_instance_pix2pix/$D_MODEL_NAME ./pretrained_models/singing_transcription/singing_transcription_pix2pix/')
        # else: #'batch' 128
        #     self.run('cp -rf ./checkpoints/singing_transcription_128/singing_transcription_128_pix2pix/$G_MODEL_NAME ./pretrained_models/singing_transcription/singing_transcription_pix2pix/')
        #     self.run('cp -rf ./checkpoints/singing_transcription_128/singing_transcription_128_pix2pix/$D_MODEL_NAME ./pretrained_models/singing_transcription/singing_transcription_pix2pix/') 
        
        if copy_model:
            checkpoints_G_models_path = './checkpoints/%s/%s_pix2pix/$G_MODEL_NAME'%(CLASS,CLASS)
            checkpoints_D_models_path = './checkpoints/%s/%s_pix2pix/$D_MODEL_NAME'%(CLASS,CLASS)
            pretrained_models_path = './pretrained_models/%s/%s_pix2pix/'%(CLASS,CLASS)
            os.makedirs(pretrained_models_path, exist_ok=True) 
            self.run('cp -rf %s %s'%(checkpoints_G_models_path,pretrained_models_path))
            self.run('cp -rf %s %s'%(checkpoints_D_models_path,pretrained_models_path))
        ##############################################
        self.run(' CUDA_VISIBLE_DEVICES=$GPU_ID python $CMD \
          $NL \
          --dataroot $DATAROOT \
          --results_dir $RESULTS_DIR \
          --checkpoints_dir $CHECKPOINTS_DIR \
          --name $NAME \
          --model $MODEL \
          --serial_batches \
          $PHASE \
          $DATASET_MODE  \
          --direction $DIRECTION \
          --load_size $LOAD_SIZE \
          --crop_size $CROP_SIZE \
          --input_nc $INPUT_NC \
          --num_test $NUM_TEST \
          $NUM_SAMPLES \
          --center_crop \
          --no_encode \
          $NO_FLIP \
          $USE_DROPOUT \
          $NET_G \
          $EPOCH \
          $CPU')
        print('[Prediction] Done ... %fs'%(time.time() - startTime))
            
    def run_chord2img(self, song_groundtruth_path):
        start_time = datetime.datetime.now()
        ##############################################################
        _path = song_groundtruth_path[0]
        mp3_path = song_groundtruth_path[1]
        groundtruth_path = song_groundtruth_path[2]
        ##############################################################
        y, sr = librosa.load(mp3_path,offset=0.0,sr=self.sample_rate)
        total_sec = librosa.get_duration(y=y, sr=self.sample_rate)
        print("%s - %s [%fs]"%(os.path.basename(mp3_path),os.path.basename(groundtruth_path),total_sec))    
        ##############################################################
        zero_chord = 'N'
        time_index = 0
        chord_pixel = []
        chord = []
        onset = []
        offset = []
        prev_chord = []
        with open(groundtruth_path,'r', newline=None) as fp:
            for line in fp.readlines():
                line = line.strip().split('\t',2)
    #             print(line)
                g_pitch_start_time = np.float64(line[0])
                g_pitch_end_time = np.float64(line[1])
                g_chord = str(line[2])
                
                onset.append(g_pitch_start_time)
                offset.append(g_pitch_end_time)
                if g_chord == prev_chord:
                    g_chord += '_SAME'
                chord.append(g_chord)
                prev_chord = g_chord
    #             print('pitch %s start:%s ~ end:%s'%(g_pitch,g_pitch_start_time,g_pitch_end_time))
                while time_index <= g_pitch_end_time:
                    if time_index >= g_pitch_start_time:
                        chord_pixel.append(g_chord)
                    else:
                        chord_pixel.append(zero_chord)
                    time_index += self.pixel_interval
        while time_index <= total_sec:
            chord_pixel.append(zero_chord)
            time_index += self.pixel_interval
        
        ###############################
        # 產出 Chord Groundtruth Image
        ###############################
        x = 0
        chord_image = np.zeros((256,len(chord_pixel),3), np.uint8)                    
        for i in chord_pixel:
            a = str(i).split('_', 2)
            # print(a)
            symble_code = self.symble_dict[a[0]]
            symble_bit_string = BitArray('0x{:0>3X}'.format(symble_code))                                            
        #   print(symble_bit_string)
            # if len(a) > 1:
            #     b = int(symble_bit_string.bin[0:4]+'1111', 2)
            #     g = int(symble_bit_string.bin[4:8]+'1111', 2)
            #     r = int(symble_bit_string.bin[8:12]+'1111', 2)
            # else:
            #     b = int(symble_bit_string.bin[0:4]+'0000', 2)
            #     g = int(symble_bit_string.bin[4:8]+'0000', 2)
            #     r = int(symble_bit_string.bin[8:12]+'0000', 2)
            b = int(symble_bit_string.bin[0:4]+'0000', 2)
            g = int(symble_bit_string.bin[4:8]+'0000', 2)
            r = int(symble_bit_string.bin[8:12]+'0000', 2)
    #       print(r,g,b)
            cv2.line(chord_image, (x, 0), (x, 255), (b,g,r), 1)
            # cv2.line(chord_image, (x, 0), (x, 255), (symble_code,symble_code,symble_code), 1)
            x += 1

        a1=os.path.basename(groundtruth_path).split('.',1)[0]
        a2=os.path.basename(mp3_path).split('.',1)[0]
        cv2.imwrite('%s/%s_%s.png'%(_path,a2,a1), chord_image)
        ##############################################################
        data = pd.DataFrame()
        data['onset'] = onset
        data['offset'] = offset
        data['chord'] = chord                    
        data.to_csv('%s/%s_chord.csv'%(_path,os.path.splitext(os.path.basename(mp3_path))[0]), index=False)
        ##############################################################
        elapsed_time = datetime.datetime.now() - start_time
        print(" %s - %s [%fs] ... [%s]"%(os.path.basename(mp3_path),os.path.basename(groundtruth_path),total_sec,elapsed_time)) 

    def chord2img(self, csvFile):
        train = pd.read_csv(csvFile)
        count = 1
        for _path, _groundtruth, _songs in zip(train['paths'],train['groundtruth'],train['songs']):
            mp3_path = os.path.join(_path, _songs)
            groundtruth_path = os.path.join(_path, _groundtruth)
            if path.exists(mp3_path) == True:
                if path.exists(groundtruth_path) == True:            
                    ##############################################################
                    y, sr = librosa.load(mp3_path,offset=0.0,sr=self.sample_rate)
                    total_sec = librosa.get_duration(y=y, sr=self.sample_rate)
                    print("[%d] %s - %s [%fs]"%(count,os.path.basename(mp3_path),os.path.basename(groundtruth_path),total_sec))  
                    count += 1    
                    ##############################################################
                    zero_chord = 'N'
                    time_index = 0
                    chord_pixel = []
                    chord = []
                    onset = []
                    offset = []
                    prev_chord = []
                    with open(groundtruth_path,'r', newline=None) as fp:
                        for line in fp.readlines():
                            line = line.strip().split('\t',2)
    #                         print(line)
                            g_pitch_start_time = np.float64(line[0])
                            g_pitch_end_time = np.float64(line[1])
                            g_chord = str(line[2])
                            
                            onset.append(g_pitch_start_time)
                            offset.append(g_pitch_end_time)

                            if g_chord == prev_chord:
                                g_chord += '_SAME'
                            chord.append(g_chord)
                            prev_chord = g_chord
    #                         print('pitch %s start:%s ~ end:%s'%(g_pitch,g_pitch_start_time,g_pitch_end_time))
                            while time_index <= g_pitch_end_time:
                                if time_index >= g_pitch_start_time:
                                    chord_pixel.append(g_chord)
                                else:
                                    chord_pixel.append(zero_chord)
                                time_index += self.pixel_interval
                    while time_index <= total_sec:
                        chord_pixel.append(zero_chord)
                        time_index += self.pixel_interval
                    
                    ###############################
                    # 產出 Chord Groundtruth Image
                    ###############################
                    x = 0
                    chord_image = np.zeros((256,len(chord_pixel),3), np.uint8)                    
                    for i in chord_pixel:
                        a = str(i).split('_', 2)
                        # print(a)
                        symble_code = self.symble_dict[a[0]]
                        symble_bit_string = BitArray('0x{:0>3X}'.format(symble_code))                                            
        #                 print(symble_bit_string)
                        # if len(a) > 1:
                        #     b = int(symble_bit_string.bin[0:4]+'1111', 2)
                        #     g = int(symble_bit_string.bin[4:8]+'1111', 2)
                        #     r = int(symble_bit_string.bin[8:12]+'1111', 2)
                        # else:
                        #     b = int(symble_bit_string.bin[0:4]+'0000', 2)
                        #     g = int(symble_bit_string.bin[4:8]+'0000', 2)
                        #     r = int(symble_bit_string.bin[8:12]+'0000', 2)

                        b = int(symble_bit_string.bin[0:4]+'0000', 2)
                        g = int(symble_bit_string.bin[4:8]+'0000', 2)
                        r = int(symble_bit_string.bin[8:12]+'0000', 2)
                            
    #                     print(r,g,b)
                        cv2.line(chord_image, (x, 0), (x, 255), (b,g,r), 1)
                        # cv2.line(chord_image, (x, 0), (x, 255), (symble_code,symble_code,symble_code), 1)
                        x += 1

                    a1=os.path.basename(groundtruth_path).split('.',1)[0]
                    a2=os.path.basename(mp3_path).split('.',1)[0]
                    cv2.imwrite('%s/%s_%s.png'%(_path,a2,a1), chord_image)
                    
                    data = pd.DataFrame()
                    data['onset'] = onset
                    data['offset'] = offset
                    data['chord'] = chord                    
                    data.to_csv('%s/%s_chord.csv'%(_path,os.path.splitext(os.path.basename(mp3_path))[0]), index=False)
                else:
                    print(" Not found Groundtruth %s"%(groundtruth_path))
            else:
                print(" Not found MP3 %s"%(mp3_path))
        print("=== Done ===")

    def check_datasets(self,csv):
        train = pd.read_csv(csv)
        for _path, _groundtruth, _songs in zip(train['paths'],train['groundtruth'],train['songs']):
            mp3_path = os.path.join(_path, _songs)
            groundtruth_path = os.path.join(_path, _groundtruth)
            if path.exists(mp3_path) == True:
                if path.exists(groundtruth_path) == True:     
                    ##############################################################
                    zero_chord = 'N'
                    time_index = 0
                    chord = []
                    onset = []
                    duration = []
                    offset = []
                    with open(groundtruth_path,'r', newline=None) as fp:
                        for line in fp.readlines():
                            line = line.strip().split('\t',2)
                            g_pitch_start_time = np.float64(line[0])
                            g_pitch_end_time = np.float64(line[1])
                            g_chord = str(line[2])
                            
                            onset.append(g_pitch_start_time)
                            offset.append(g_pitch_end_time)
                            chord.append(g_chord)
                            duration.append(g_pitch_end_time-g_pitch_start_time)


                    data = pd.DataFrame()
                    data['chord'] = chord 
                    data['onset'] = onset
                    data['offset'] = offset
                    data['duration'] = duration
                    data.to_csv('%s/%s_chord.csv'%(_path,os.path.splitext(os.path.basename(mp3_path))[0]), index=False)
                    print('Done %s_chord.csv'%(os.path.splitext(os.path.basename(mp3_path))[0]))
                else:
                    print(" Not found Groundtruth %s"%(groundtruth_path))
            else:
                print(" Not found MP3 %s"%(mp3_path))
        print('=== Done ===')        

    def gen_all_chord_csv(self,):
        chord_path = Path(self.source_data_dir_path).glob('*/*_chord.csv')
        duration = []
        chord = []
        song_path=[]
        for p in chord_path:
            csv_data = pd.read_csv(p)
            for _onset, _offset, _chord in zip(csv_data['onset'],csv_data['offset'],csv_data['chord']):        
                duration.append(_offset-_onset)
                chord.append(_chord)
                song_path.append(str(p))
                
        all_chord_data = pd.DataFrame()
        all_chord_data['song'] = song_path
        all_chord_data['chord'] = chord
        all_chord_data['duration'] = duration
        
    #     all_chord_all_data['duration'].describe()
    #     all_chord_all_data['chord'].value_counts()
        all_chord_data.to_csv('all_data_chord.csv', index=False)
        print('=== Done ===')  

    def gen_chord_symble(self,csvFile='all_data_chord.csv',max_code=0xFFF,div_value=7):
        csv = pd.read_csv(csvFile)
        
        # 賦予**[和弦]**一個代碼，範圍 0~0xFFF
        df_chord = csv[csv['chord'] != 'N']
        frequency_df = df_chord['chord'].value_counts()
        symbol_list = frequency_df.index.tolist()
        symbol_list.sort(reverse=True)
        code_list = []
        chord_code = max_code
        for i in symbol_list:    
            code_list.append(chord_code)
            chord_code -= div_value
        code_list.append(0)
        symbol_list.append('N')

        chord_dict = {'chord':symbol_list, 'code':code_list}
        data = pd.DataFrame(chord_dict)
        data.to_csv(os.path.join('./', 'chord_symble.csv'), index=False)        