from __future__ import print_function, division
import os
from PIL import Image
import numpy as np
from torch.utils.data import Dataset
from mypath import Path
from torchvision import transforms
from dataloaders import custom_transforms as tr
from glob import glob
import random



class CE200Segmentation(Dataset):
    """
    CE200 dataset
    """
    # NUM_CLASSES = 19
    NUM_CLASSES = 540

    def __init__(self,
                 args,
                 base_dir=Path.db_root_dir('CE200'),
                 split='train',
                 ):
        """
        :param base_dir: path to CE200 dataset directory
        :param split: train/val
        :param transform: transform to apply
        """
        super().__init__()
        self._base_dir = base_dir
        if isinstance(split, str):
            self.split = [split]
        else:
            split.sort()
            self.split = split

        self.args = args
        print(os.path.join(self._base_dir,split))
        self.images = []
        self.categories = []
        for _path_ in glob('%s/*_A.png'%(os.path.join(self._base_dir,split))):
            self.images.append(_path_)
        for _path_ in glob('%s/*_B.png'%(os.path.join(self._base_dir,split))):
            self.categories.append(_path_)

        self.images.sort()
        self.categories.sort()

        assert (len(self.images) == len(self.categories))

        # Display stats
        print('Number of images in {}: {:d}'.format(split, len(self.images)))


    def __len__(self):
        return len(self.images)


    def __getitem__(self, index):
        _img, _target = self._make_img_gt_point_pair(index)
        sample = {'image': _img, 'label': _target}

        for split in self.split:
            if split == "train":
                return self.transform_tr(sample)
            elif split == 'val':
                return self.transform_val(sample)

    def _make_img_gt_point_pair(self, index):
        _img = Image.open(self.images[index]).convert('RGB')
        # _target = Image.open(self.categories[index])

        # nb_classes = 19 - 1 # 18 classes + background
        # idx = np.linspace(0., 1., nb_classes)
        # cmap = matplotlib.cm.get_cmap('viridis')
        # rgb = cmap(idx, bytes=True)[:, :3]  # Remove alpha value

        # h, w = 190, 100
        # rgb = rgb.repeat(1000, 0)
        # target = np.zeros((h*w, 3), dtype=np.uint8)
        # target[:rgb.shape[0]] = rgb
        # target = target.reshape(h, w, 3)

        # plt.imshow(target) # Each class in 10 rows


        # # Create mapping
        # # Get color codes for dataset (maybe you would have to use more than a single
        # # image, if it doesn't contain all classes)
        # target = torch.from_numpy(target)
        # colors = torch.unique(target.view(-1, target.size(2)), dim=0).numpy()
        # target = target.permute(2, 0, 1).contiguous()

        # mapping = {tuple(c): t for c, t in zip(colors.tolist(), range(len(colors)))}

        # mask = torch.empty(h, w, dtype=torch.long)
        # for k in mapping:
        #     # Get all indices for current class
        #     idx = (target==torch.tensor(k, dtype=torch.uint8).unsqueeze(1).unsqueeze(2))
        #     validx = (idx.sum(0) == 3)  # Check that all channels match
        #     mask[validx] = torch.tensor(mapping[k], dtype=torch.long)

        # aaa = Image.open(self.categories[index]).convert('RGB')
        # print(index,self.categories[index],aaa.size,aaa.mode)
        # _tmp = np.array(Image.open(self.categories[index]).convert('RGB'), dtype=np.uint8)
        # _tmp = self.encode_segmap(_tmp)
        # _target = Image.fromarray(_tmp)

        # _target = Image.open(self.categories[index]).convert('L')
        _target = Image.open(self.categories[index]).convert('L')
        # r, g, b = _target.split() 
        # r_plane = (np.array(r, dtype=np.uint32)& 0xF0)<<16   
        # g_plane = (np.array(g, dtype=np.uint32)& 0xF0)<<8
        # b_plane = np.array(b, dtype=np.uint32)& 0xF0   
   
        # color_code = r_plane+g_plane+b_plane
        # print(color_code.shape)
        # _b_ = BitArray('0x{:0>2X}'.format(int(b_plane))).bin[0:4]
        # _g_ = BitArray('0x{:0>2X}'.format(int(g_plane))).bin[0:4]
        # _r_ = BitArray('0x{:0>2X}'.format(int(r_plane))).bin[0:4]

        # _target = np.array(_tmp, dtype=np.uint8)
        # _target = np.array(_tmp, dtype=np.uint8)
        # _target = np.zeros((_target.size[0],_target.size[1]), np.int32)
        # _target = np.zeros((_target.size[0],_target.size[1]), np.float32)
        # _target +=  random.randint(0,541)
    
        # _target = Image.fromarray(_target).convert('F') # (32-bit floating point pixels)
        # _target = Image.fromarray(color_code).convert('F') # (32-bit floating point pixels)
        # _target = Image.fromarray(color_code).convert('I') # (32-bit signed integer pixels)
        # _target = Image.fromarray(_target).convert('RGB')

        # print(index,self.categories[index],_target.size,_target.mode)

        return _img, _target

    def _gen_seg_mask(self, target, h, w):
        mask = np.zeros((h, w), dtype=np.uint8)
        coco_mask = self.coco_mask
        for instance in target:
            rle = coco_mask.frPyObjects(instance['segmentation'], h, w)
            m = coco_mask.decode(rle)
            cat = instance['category_id']
            if cat in self.CAT_LIST:
                c = self.CAT_LIST.index(cat)
            else:
                continue
            if len(m.shape) < 3:
                mask[:, :] += (mask == 0) * (m * c)
            else:
                mask[:, :] += (mask == 0) * (((np.sum(m, axis=2)) > 0) * c).astype(np.uint8)
        return mask

    def encode_segmap(self, mask):
        # Put all void classes to zero
        # for _voidc in self.void_classes:
        #     mask[mask == _voidc] = self.ignore_index
        # for _validc in self.valid_classes:
        #     mask[mask == _validc] = self.class_map[_validc]
        return mask

    def transform_tr(self, sample):
        composed_transforms = transforms.Compose([
            tr.RandomHorizontalFlip(),
            tr.RandomScaleCrop(base_size=self.args.base_size, crop_size=self.args.crop_size),
            tr.RandomGaussianBlur(),
            tr.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225)),
            tr.ToTensor()])

        return composed_transforms(sample)

    def transform_val(self, sample):

        composed_transforms = transforms.Compose([
            tr.FixScaleCrop(crop_size=self.args.crop_size),
            tr.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225)),
            tr.ToTensor()])

        return composed_transforms(sample)

    def __str__(self):
        return 'CE200(split=' + str(self.split) + ')'


if __name__ == '__main__':
    from dataloaders.utils import decode_segmap
    from torch.utils.data import DataLoader
    import matplotlib.pyplot as plt
    import argparse

    parser = argparse.ArgumentParser()
    args = parser.parse_args()
    # args.base_size = 256
    # args.crop_size = 256
    args.base_size = 513
    args.crop_size = 513

    CE200_train = CE200Segmentation(args, split='train')

    dataloader = DataLoader(CE200_train, batch_size=5, shuffle=True, num_workers=0)

    for ii, sample in enumerate(dataloader):
        for jj in range(sample["image"].size()[0]):
            img = sample['image'].numpy()
            gt = sample['label'].numpy()
            tmp = np.array(gt[jj]).astype(np.uint8)
            segmap = decode_segmap(tmp, dataset='CE200')
            img_tmp = np.transpose(img[jj], axes=[1, 2, 0])
            img_tmp *= (0.229, 0.224, 0.225)
            img_tmp += (0.485, 0.456, 0.406)
            img_tmp *= 255.0
            img_tmp = img_tmp.astype(np.uint8)
            plt.figure()
            plt.title('display')
            plt.subplot(211)
            plt.imshow(img_tmp)
            plt.subplot(212)
            plt.imshow(segmap)

        if ii == 1:
            break

    plt.show(block=True)


