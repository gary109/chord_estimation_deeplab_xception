import secrets
from skimage.morphology import medial_axis, skeletonize

from collections import deque
import multiprocessing
from bitstring import BitArray
import progressbar
import librosa, librosa.display
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import IPython.display as ipd
import numpy as np
import pandas as pd
import sklearn
from sklearn import preprocessing
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from skimage.transform import rescale, resize, downscale_local_mean
import cv2
from glob import glob
# from time import time
import time
import shutil
import os
import scipy
from scipy import signal
from scipy import stats
# from scipy.misc import imsave
import datetime
import math
from pathlib import Path
import youtube_dl
import argparse
import sys
import mido
import json
import os.path
from os import path
import seaborn as sns
import copy
import PIL
from scipy import misc

import random

from madmom.audio.filters import LogarithmicFilterbank
from madmom.features.onsets import SpectralOnsetProcessor
from madmom.audio.signal import normalize

# import parselmouth

from midiutil.MidiFile import MIDIFile
from pandas.core.frame import DataFrame

from multiprocessing import Pool
import multiprocessing as mp

from collections import deque
from pathlib import Path 
from sklearn.utils import shuffle


sns.set() # Use seaborn's default style to make attractive graphs

kernel84 = np.array(([1, 2, 1],
                     [2,-12, 2],
                     [1, 2, 1]), dtype="float32")

kernel4 = np.array(([0, 1, 0],
                    [1,-4, 1],
                    [0, 1, 0]), dtype="float32")

kernel8 = np.array(([-1, -1, -1],
                    [-1, 8, -1],
                    [-1, -1, -1]), dtype="float32")

def rgb2gray(rgb):
    return np.dot(rgb[...,:3], [0.299, 0.587, 0.114])
    # return np.dot(rgb[...,:3], [0.333, 0.333, 0.333]) 
    # return np.dot(rgb[...,:3], [0.9, 0.9, 0.9]) 

def gamma(rgb,simga=1/1.5):
    return np.power(rgb/float(np.max(rgb)), simga)

###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################        
class Chord:        
    def __init__(self):
        pass
###############################################################################################
###############################################################################################    
###############################################################################################
############################################################################################### 
    def color2chord(self,color,df_symble,th_chord=0.5):
        # gray_data = self.rgb2gray(color)
        # df = DataFrame(gray_data)

        # B
        b_plane = color[:,:,0]    
        # G
        g_plane = color[:,:,1]   
        # R
        r_plane = color[:,:,2]  

        df_b = DataFrame(b_plane)
        df_g = DataFrame(g_plane)
        df_r = DataFrame(r_plane)

        chord_list = []
        chord_row_index = []
        for i in range(df_b.shape[1]):
            frequency_df_b = df_b[i].value_counts(normalize=True, ascending=False)
            frequency_df_g = df_g[i].value_counts(normalize=True, ascending=False)
            frequency_df_r = df_r[i].value_counts(normalize=True, ascending=False)
            
            norm_list_b = frequency_df_b.tolist()
            norm_list_g = frequency_df_g.tolist()
            norm_list_r = frequency_df_r.tolist()
                    
            frequency_list_b = frequency_df_b.index.tolist()
            frequency_list_g = frequency_df_g.index.tolist()
            frequency_list_r = frequency_df_r.index.tolist()

            chord_int = 0
            chord_row_index = df_symble.shape[0]-1
            if norm_list_b[0] >= th_chord and norm_list_g[0] >= th_chord and norm_list_r[0] >= th_chord:
                B = math.ceil(frequency_list_b[0])
                G = math.ceil(frequency_list_g[0])
                R = math.ceil(frequency_list_r[0])

                b_bit_string = BitArray('0x{:0>2X}'.format(int(B))) 
                g_bit_string = BitArray('0x{:0>2X}'.format(int(G)))
                r_bit_string = BitArray('0x{:0>2X}'.format(int(R)))

                chord_bit_string = b_bit_string.bin[0:4]+g_bit_string.bin[0:4]+r_bit_string.bin[0:4]
                chord_int = int(chord_bit_string, 2)
                # chord_row_index = int(float(4095 - chord_int)/7)-1
                _index_ = df_symble[df_symble.code.isin([chord_int])].index
               
                if _index_.size > 0:
                    chord_row_index = int(_index_[0])
                else: # 四捨五入 最接近
                    # print('chord_int:',chord_int)
                    if chord_int <= 4095 or chord_int >= 322:
                        chord_row_index = int(round(float(chord_int)/7)*7)
                    

            # print('chord_row_index: ',chord_row_index,_index_)
            
            if chord_row_index < 0:
                chord_row_index = df_symble.shape[0]-1
                # print('chord_row_index < 0')
            elif chord_row_index >= df_symble.shape[0]:
                chord_row_index = df_symble.shape[0]-1
                # print('chord_row_index >= df_symble.shape[0]')
            chord_symble = df_symble.iloc[int(chord_row_index)]['chord']
            # print('[TEST] ',chord_row_index,chord_symble)
            chord_list.append(chord_row_index)
        return chord_list        
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################        
    def result2chord(self, song_id, out_path, result_dir='./results/predict/images'):   
        ###########################################################################
        # Step1. 將一首歌曲的預測結果圖，水平合併成一張 256(長)x 歌曲長度(寬) x 3(RGB)
        ###########################################################################
        import time as ts
        startTime = ts.time()
        startTotalTime = ts.time()
        img_index = 1
        data = []

        while True:
            file_path = '%s/input_%d_%d_fake_B.png'%(result_dir,song_id,img_index)
            if path.exists(file_path) == True:
                inputs = cv2.imread(file_path)
                if img_index == 1:
                    data = inputs
                else:
                    data = np.hstack((data, inputs))
    
            elif path.exists(file_path) == False and img_index == 1:
                print('song_id:%d not exist!!!'%(song_id))
                return []
            else:
                break
            img_index += 1     

    
        print('[result2chord] Step1 Done ... %.3fs'%(ts.time() - startTime))    
        
        startTime = ts.time()
        cv2.imwrite('%s/%s_predict_chords.png'%(out_path,song_id), data) 
        chord_list = self.color2chord(data,self.df_symble)
        print('[result2chord] Step2 Done ... %.3fs'%(ts.time() - startTime))
        np.savez('%s/%s_chord_list.npz'%(out_path,song_id), chord_list=chord_list)

###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################        
    def result2chord2(self, song_id, out_path, result_dir='./results/predict/images'):   
        ###########################################################################
        # Step1. 將一首歌曲的預測結果圖，水平合併成一張 256(長)x 歌曲長度(寬) x 3(RGB)
        ###########################################################################
        import time as ts
        startTime = ts.time()
        startTotalTime = ts.time()
        img_index = 1
        data = []
        while True:
            file_path = '%s/input_%d_%d_fake_B.png'%(result_dir,song_id,img_index)
            if path.exists(file_path) == True:
                inputs = cv2.imread(file_path)
                if img_index == 1:
                    data = inputs
                else:
                    data = np.hstack((data, inputs))
    
            elif path.exists(file_path) == False and img_index == 1:
                print('song_id:%d not exist!!!'%(song_id))
                return []
            else:
                break
            img_index += 1             
        cv2.imwrite('%s/%s_predict_chords.png'%(out_path,song_id), data) 
        print('[result2chord] Done ... %.3fs'%(ts.time() - startTime))    
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################        
    def chord2csv(self,out_path,song_id):  
        df_chord = pd.read_csv('chord_symble.csv', usecols = ["chord"], squeeze = True)
        # df_pattern_size = pd.DataFrame({'size':['3L', '2L', 'XL', 'L', 'M', 'S','XS','2S','3S']}) 

        # Loading NPZ ...
        # song_chord_list = np.load('%s/%s_chord_list.npz'%(out_path,song_id))['chord_list']
        song_chord_note = np.load('%s/%s_chord_note.npz'%(out_path,song_id))['note']

        # Chord Note ...
        cT = np.array(song_chord_note).T  
        df_chord_note = pd.DataFrame({'chord':cT[2],'onset':cT[0],'offset':cT[1],'time':np.array(cT[1])-np.array(cT[0])}) 
        print('df_predict_chord shape:',df_chord_note.shape)
        df_chord_note['chord_name'] = df_chord_note['chord'].map(df_chord) 
        df_chord_note.to_csv('%s/%s_chord_note.csv'%(out_path,song_id),index=False)

        # Chord List ...
        # df_chord_list = pd.DataFrame({'chord':song_chord_list})
        # print('df_chord_list shape:',df_chord_list.shape)
        # df_chord_list['chord_name'] = df_chord_list['chord'].map(df_chord) 
        # df_chord_list.to_csv('%s/%s_chord_list.csv'%(out_path,song_id),index=False)

###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################        
    def chord2note(self,song_id,out_path,shift_ms=0, toMIDI=True, filter_noise_ms=60):  
        import time as ts
        startTime = ts.time()


        chord_list_npz = np.load('%s/%s_chord_list.npz'%(out_path,song_id))
        chord_list = chord_list_npz['chord_list'].astype('float').tolist()

        # # Step 3.1 Shift pitch onset/offset
        # if shift_ms != 0:
        #     shift_pixel = int(math.fabs(shift_ms)/1000/self.pixel_interval)
        #     for i in range(shift_pixel):
        #         if shift_ms > 0:
        #             chord_list.insert(0, np.int(631))
        #             chord_list.pop(-1)
        #         else:
        #             chord_list.pop(0)
        #             chord_list.append(np.int(631))

        # #=======================================
        # # outputing chord_note list
        # #=======================================
        # for num, _chord in enumerate(chord_list, start=0):
        #     if _chord == 'N':  
        #         chord_list[num] = '0'

        time = 0
        chord_note_list = []
        prev_chord = chord_list[0]
        now_chord = chord_list[0]
        onset=0
        offset=0
        index = 0
        for _chord in chord_list:
            now_chord = _chord
            # if self.verbose:
            #     print('[%d] now_chord:%s, prev_chord:%s'%(index,now_chord,prev_chord))
            index += 1
            if now_chord != prev_chord:
                # chord_note_list.append((onset, time, prev_chord))
                _onset = onset
                _offset = time
                duration = _offset - _onset
                time_onset = round(_onset*self.pixel_interval, 3)  
                time_offset = round(_offset*self.pixel_interval, 3) 
                time_duration = round(duration*self.pixel_interval, 3)*1000     
                if time_duration >= filter_noise_ms:# and prev_chord != 631: # About 64ms = 0.016*
                    chord_note_list.append([float(time_onset), float(time_offset), prev_chord])
                onset = time+1
            prev_chord = now_chord
            time += 1                

        # os.makedirs('./midi/', exist_ok=True)    
        # outmidi_path = './midi'
        np.savez('%s/%s_chord_note.npz'%(out_path,song_id), note=chord_note_list)
        print('[chord2note] Done ... %.3fs'%(ts.time() - startTime))
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################  
    #空間距離權重模板
    def getClosenessWeight(self,sigma_g, H, W):
        
        r, c = np.mgrid[0:H:1, 0:W:1] #有兩部分。[0]是y軸的範圍。[1]是x軸的範圍
        #模板中心
        cH = (H - 1) // 2
        cW = (W - 1) // 2
        r -= cH
        c -= cW
        closeWeight = np.exp(-0.5*(np.power(r,2)+np.power(c,2))/math.pow(sigma_g,2)) #sigma_g空間距離權重模板標準差
        
        return closeWeight
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################  
    def jointBLF(self,I, H, W, sigma_g, sigma_d, borderType=cv2.BORDER_DEFAULT):
        rows, cols = I.shape
        I = I.astype(np.float64)
        #構建空間距離模板
        closenessWeight = self.getClosenessWeight(sigma_g, H, W)
        #對I進行平滑
        Ig = cv2.GaussianBlur(I, (W,H), sigma_g)
        #模板中心位置 ,(H x W) 是模板的大小
        cH = (H - 1) // 2
        cW = (W - 1) // 2
        #對原圖和高斯平滑的結果擴充邊界
        Ip = cv2.copyMakeBorder(I, cH, cH, cW, cW, borderType)
        Igp = cv2.copyMakeBorder(Ig, cH, cH, cW, cW, borderType)
        #影象矩陣的行、列數
        
        i, j = 0, 0
        #聯合雙邊濾波的結果
        jblf = np.zeros(I.shape, np.float64)
        
        for r in np.arange(cH, cH+rows, 1):
            for c in np.arange(cW, cW+cols, 1):
                #當前的位置的值
                pixel = Igp[r][c]
                #當前位置的鄰域
                rTop, rBottom, cLeft, cRight = r-cH, r+cH, c-cW, c+cW
                #從Igp中擷取該鄰域，用於構建相似性權重模板
                region = Igp[rTop:rBottom+1, cLeft:cRight+1]
                #用上述鄰域，構建該位置的相似性模板
                similarityWeight = np.exp(-0.5*np.power(region-pixel,2.0)/math.pow(sigma_d,2.0))
                #兩個模板相乘
                Weigth = closenessWeight * similarityWeight
                #將權重模板歸一化
                Weigth = Weigth / np.sum(Weigth)
                #權重模板和鄰域相對應的位置相乘並求和
                jblf[i][j] = np.sum(Ip[rTop:rBottom+1, cLeft:cRight+1]*Weigth)
                
                j += 1
                
            j = 0
            i += 1
        
        return jblf

###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################  
    # 分水岭算法
    def water_image(self,src,out_path):
        print(src.shape)
        blurred = cv2.pyrMeanShiftFiltering(src, 10, 100)    # 去除噪点
    
        # gray\binary image
        gray = cv2.cvtColor(blurred, cv2.COLOR_BGR2GRAY)
        ret, binary = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
        # cv.imshow("二值图像", binary)
        cv2.imwrite('%s/improc/water1.png'%(out_path), binary)   
    
        # morphology operation
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
        mb = cv2.morphologyEx(binary, cv2.MORPH_OPEN, kernel, iterations=2)
        sure_bg = cv2.dilate(mb, kernel, iterations=3)
        # cv.imshow("形态操作", sure_bg)
        cv2.imwrite('%s/improc/water2.png'%(out_path), sure_bg)   
    
        # distance transform
        dist = cv2.distanceTransform(mb, cv2.DIST_L2, 3)
        dist_output = cv2.normalize(dist, 0, 1.0, cv2.NORM_MINMAX)
        # cv.imshow("距离变换", dist_output*70)
        cv2.imwrite('%s/improc/water3.png'%(out_path), dist_output*70)  
    
        ret, surface = cv2.threshold(dist, dist.max()*0.6, 255, cv2.THRESH_BINARY)
        # cv.imshow("寻找种子", surface)
        cv2.imwrite('%s/improc/water4.png'%(out_path), surface)
    
        surface_fg = np.uint8(surface)
        unknown = cv2.subtract(sure_bg, surface_fg)
        ret, markers = cv2.connectedComponents(surface_fg)
        # print(ret)
    
        # watershed transfrom
        markers += 1
        markers[unknown == 255] = 0
        markers = cv2.watershed(src, markers=markers)
        src[markers == -1] = [0, 0, 255]
        # cv.imshow("分水岭结果", src)
        cv2.imwrite('%s/improc/water5.png'%(out_path), src)

###############################################################################################
###############################################################################################    
 # cv2.imwrite('%s/0.png'%(improc_path), img_predict) 
# blurred = cv2.pyrMeanShiftFiltering(img_predict, 10, 100)    # 去除噪点
# res1 = cv2.pyrMeanShiftFiltering(img_predict, 10, 100)    # 去除噪点
# cv2.imwrite('%s/1.png'%(improc_path), res) 
        
 
    
        # blurred = np.vstack([
        #     cv2.bilateralFilter(img_predict,5,21,21),
        #     cv2.bilateralFilter(img_predict,7, 31, 31),
        #     cv2.bilateralFilter(img_predict,9, 41, 41),
        #     cv2.bilateralFilter(img_predict, 0, 50, 50),
        #     cv2.bilateralFilter(img_predict, 0, 100, 50),
        #     cv2.bilateralFilter(img_predict, 5, 100, 50),
        #     cv2.bilateralFilter(img_predict, 5, 200, 100),])
        # cv2.imwrite('%s/blurred.png'%(improc_path), blurred)

        # #將8點陣圖轉為浮點型
        # # fImage = img_predict.astype(np.float64)
        
        # #聯合雙邊濾波返回值為浮點型
        # jblf = self.jointBLF(img_predict, 5, 5, 7, 2)
        # #再轉為8點陣圖
        # jblf = np.round(jblf)
        # jblf = jblf.astype(np.uint8)
        # cv2.imwrite('%s/03.png'%(improc_path), jblf) 

        
        # res = cv2.medianBlur(img_predict, 49)

        # res = img_predict
        # cv2.imwrite('%s/0.png'%(improc_path), res) 

        # res = cv2.GaussianBlur(img_predict,(3, 3), 0)
        # cv2.imwrite('%s/improc/1.png'%(out_path), res) 
        # res = cv2.filter2D(res, -1, kernel84) 
        # cv2.imwrite('%s/improc/2.png'%(out_path), res)  
        # res = rgb2gray(res)
        # cv2.imwrite('%s/improc_3.png'%(out_path), res)  
        # res = cv2.GaussianBlur(img_predict,(3, 3), 0) - res
        # alpha = 1.0
        # beta = 80
        # res = np.uint8(np.clip((alpha * img_predict + beta), 0, 255))
        # cv2.imwrite('%s/improc/1.png'%(out_path), res)   

        
        
        # cv2.imwrite('%s/improc_6.png'%(out_path), res)  
        
        # cv2.imwrite('%s/improc/1.png'%(out_path), res) 

        # out_x = cv2.Sobel(res, cv2.CV_16S, 1, 0)    # x gradient
        # out_y = cv2.Sobel(res, cv2.CV_16S, 0, 1)    # y gradient


        # out_x = cv2.convertScaleAbs(out_x)
        # out_y = cv2.convertScaleAbs(out_y)

        # out_weight = cv2.addWeighted(res, 0.5, out_x, 0.5,0)  # x and y weighted
        # cv2.imwrite('%s/improc/out_weight.png'%(out_path), out_x) 
#
        # alpha = 1.0
        # beta = 80
        # res = np.uint8(np.clip((alpha * img_predict + beta), 0, 255))
        # cv2.imwrite('%s/improc/1.png'%(out_path), res)  

        # res = cv2.GaussianBlur(res,(9, 9), 0)
        # cv2.imwrite('%s/improc/2.png'%(out_path), res) 



        # res = rgb2gray(img_predict)
        # res = cv2.GaussianBlur(res,(3, 3), 0)
        # cv2.imwrite('%s/improc/2.png'%(out_path), res) 
        # low_threshold = 30#1
        # high_threshold =50#10
        # res = cv2.Canny(np.uint8(res), low_threshold, high_threshold)
        # cv2.imwrite('%s/improc/4.png'%(out_path), res) 

# res = cv2.addWeighted(res12, 0.9, res34, 0.9, 0)
# self.water_image(res12,out_path)
        # cv2.imwrite('%s/2.png'%(improc_path), res)         
        # cv2.imwrite('%s/3.png'%(improc_path), res)
        # cv2.imwrite('%s/4.png'%(improc_path), res) 
        # cv2.imwrite('%s/5.png'%(improc_path), res) 
        # cv2.imwrite('%s/6.png'%(improc_path), res) 
        # cv2.imwrite('%s/7.png'%(improc_path), res) 
        # cv2.imwrite('%s/8.png'%(improc_path), res) 
        # res_data = np.vstack((res,res))
        # res_data = np.vstack((res_data,res_data))
        # res_data = np.vstack((res_data,res_data))
        # cv2.imwrite('%s/9.png'%(improc_path), res) 

        # blurred = np.vstack([
        #     res_i,
        #     cv2.bilateralFilter(img_predict,7, 31, 31),
        #     cv2.bilateralFilter(img_predict,9, 41, 41),
        #     cv2.bilateralFilter(img_predict, 0, 50, 50),
        #     cv2.bilateralFilter(img_predict, 0, 100, 50),
        #     cv2.bilateralFilter(img_predict, 5, 100, 50),
        #     cv2.bilateralFilter(img_predict, 5, 200, 100),])
        # cv2.imwrite('%s/blurred.png'%(improc_path), blurred)

###############################################################################################
###############################################################################################  
###############################################################################################
############################################################################################### 
    def imhmax(self, im, th) :
        im_out = self.imreconstruct(im - th, im)
        return im_out
###############################################################################################
###############################################################################################  
###############################################################################################
###############################################################################################  
    def imreconstruct(self,marker, mask, SE=np.ones([3,3])):
        """
        描述：以mask为约束，连续膨胀marker，实现形态学重建，其中mask >= marker
        
        参数：
            - marker 标记图像，单通道/三通道图像
            - mask   模板图像，与marker同型
            - conn   联通性重建结构元，参照matlab::imreconstruct::conn参数，默认为8联通
        """
        while True:
            marker_pre = marker
            dilation = cv2.dilate(marker, kernel=SE)
            marker = np.min((dilation, mask), axis=0)
            if (marker_pre == marker).all():
                break
        return marker      
# ###############################################################################################
# ###############################################################################################  
# ###############################################################################################
# ############################################################################################### 
#     def imreconstruct1(marker: np.ndarray, mask: np.ndarray, radius: int = 1):
#     """Iteratively expand the markers white keeping them limited by the mask during each iteration.

#     :param marker: Grayscale image where initial seed is white on black background.
#     :param mask: Grayscale mask where the valid area is white on black background.
#     :param radius Can be increased to improve expansion speed while causing decreased isolation from nearby areas.
#     :returns A copy of the last expansion.
#     Written By Semnodime.
#     """
#     kernel = np.ones(shape=(radius * 2 + 1,) * 2, dtype=np.uint8)
#     while True:
#         expanded = cv2.dilate(src=marker, kernel=kernel)
#         cv2.bitwise_and(src1=expanded, src2=mask, dst=expanded)

#         # Termination criterion: Expansion didn't change the image at all
#         if (marker == expanded).all():
#             return expanded
#         marker = expanded
###############################################################################################
###############################################################################################  
###############################################################################################
###############################################################################################          
    def chord2note2(self,song_id,out_path,filterSec=0.06):  
        import time as ts
        startTime = ts.time()
        improc_path = '%s/improc'%(out_path)
        chords_path = '%s/chords'%(out_path)
        path_list = [improc_path, chords_path]
        for pa in path_list:
            try:
                shutil.rmtree(pa)
            except OSError as e:
                print(e)
            else:
                print("The %s directory is deleted successfully"%(pa))
            os.makedirs('%s' % (pa), exist_ok=True)

        res_list = []

        img_predict = cv2.imread('%s/%d_predict_chords.png'%(out_path,song_id))
#         img_predict &= 0xF0
        all_improc = np.vstack([img_predict,])
       
       
        # res = cv2.bilateralFilter(img_predict, 5, 100, 50)
        # res = cv2.bilateralFilter(img_predict, 0, 100, 50)
        # res = cv2.bilateralFilter(img_predict, 3, 50, 50)
        res = cv2.bilateralFilter(img_predict, 9, 100, 100)
        all_improc = np.vstack([all_improc,res])
        

        res = res[32:-32,:]
        kernelSobelX90 = np.array([[-1,0,1], [-2,0,2], [-1,0,1]])*9
        kernelSobelX180 = np.array([[1,0,-1], [2,0,-2], [1,0,-1]])*9
        kernel8 = np.array([[-1,-1,-1], [-1,8,-1], [-1,-1,-1]])*9
        kernel4 = np.array([[0,-1,0], [-1,4,-1], [0,-1,0]])*9
        
        res1 = cv2.filter2D(res, -1, kernel8)
        res2 = cv2.filter2D(res, -1, kernel4)
        res3 = cv2.filter2D(res, -1, kernelSobelX90)
        res4 = cv2.filter2D(res, -1, kernelSobelX180)
        res12 = cv2.addWeighted(res1, 0.5, res2, 0.5,0)
        res34 = cv2.addWeighted(res3, 0.5, res4, 0.5,0)
        all_improc = np.vstack([all_improc,res12,res34])
        res = res34
        cv2.imwrite('%s/all_improc1.png'%(improc_path), all_improc)

        # ===================================================================
        res = res[32:-32,:]
        res = rgb2gray(res)     
        all_improc = np.vstack([res])  
        # ===================================================================
        # for i in range(2):
        #     kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(5, 5))  
        #     # res_imhmax = self.imhmax(res,80)
        #     res9 = cv2.erode(res,kernel,iterations = 3)
        #     res = self.imreconstruct(res9,res)
        # all_improc = np.vstack([all_improc,res])   
        # ===================================================================
        # res = cv2.GaussianBlur(res,(3, 3), 0)
        # all_improc = np.vstack([all_improc,res]) 
        # ===================================================================
        ret1,res = cv2.threshold(np.uint8(res),0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
        all_improc = np.vstack([all_improc,res])        
        # ===================================================================
        res = cv2.medianBlur(res, 3)
        all_improc = np.vstack([all_improc,res]) 
        # ===================================================================
        # kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(3, 1))  
        # for i in range(5):            
        #     res9 = cv2.erode(res,kernel,iterations = 1)
        #     res = self.imreconstruct(res9,res)
        # all_improc = np.vstack([all_improc,res])   
        # ===================================================================
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(3, 3))  
        for i in range(5):            
            res9 = cv2.dilate(res,kernel,iterations = 1)
            res = self.imreconstruct(res9,res)
        all_improc = np.vstack([all_improc,res])   
        # ===================================================================
        # kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(3, 3)) 
        # res_op = res       
        # for i in range(2): 
        #     opening = cv2.morphologyEx(res_op, cv2.MORPH_OPEN, kernel)
        #     res_op = self.imreconstruct(opening,res_op)
        # res_cl = res   
        # for i in range(2):
        #     closing = cv2.morphologyEx(res_cl, cv2.MORPH_CLOSE, kernel)
        #     res_cl = self.imreconstruct(closing,res_cl)
        # all_improc = np.vstack([all_improc,res_op,res_cl])
        # ===================================================================
        # kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(3, 3*49))  
        # res = cv2.dilate(res,kernel,iterations = 3)
        # kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(3, 3))
        # res = cv2.erode(res,kernel,iterations = 3)
        # all_improc = np.vstack([all_improc,res])
        # ===================================================================        
        res = res[32:-32,:]
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(1, 128))  
        res = cv2.dilate(res,kernel,iterations = 1)
        all_improc = np.vstack([all_improc,res])
        # ===================================================================
        # Compare with other skeletonization algorithms
        binarr = np.where(res>128, 1, 0)
        skeleton = skeletonize(binarr)
        res = np.where(skeleton>=1, 255, 0)
        all_improc = np.vstack([all_improc,res])
        # ===================================================================

        cv2.imwrite('%s/all_improc2.png'%(improc_path), all_improc)
        
        res = res[20:-20,:]
        chord_onset_offset_list = []
        _onset_counts = 0
        max_now = 0
        max_prev = 0
        time = 0
        onset = 0
        offset = 0
        for num, rows in enumerate(res.T, start=1):
            max_now = np.max(rows)
            if max_prev==0 and max_now == 255:
                offset = time
                _onset_counts += 1
                # print('[%d] _onset_counts:%d'%(num,_onset_counts))
                chord_onset_offset_list.append((onset,offset,offset-onset))
                onset = time
            max_prev = max_now
            # new_resize_img.append(max_now)
            time += 0.025
        chord_onset_offset_list.append((onset,time,time-onset))
        
        print('chord counts: ',len(chord_onset_offset_list))
        df_symble = pd.read_csv('chord_symble.csv') 
        chord_note = []
        verbose = 0
        # res_blur = cv2.medianBlur(img_predict, 49)
        for num, t in enumerate(chord_onset_offset_list, start=0):
            onset_index = int(t[0]/0.025)
            offset_index = int(t[1]/0.025)
            # print(t[0],t[1],t[2])
            if t[2] < filterSec:
                chord_note.append((t[0],t[1],df_symble.shape[0]-1))
                continue
            # print(t[0],t[1],onset_index,offset_index)
            
            start_index = onset_index + int((offset_index-onset_index)*0.1)
            end_index = onset_index + int((offset_index-onset_index)*0.9)
            
            # res = img_predict[32:192,start_index:end_index]
            res = img_predict[int(256*0.1):int(256*0.9),start_index:end_index]
            cv2.imwrite('%s/%d.png'%(chords_path,num), res)  

        #     res = cv2.GaussianBlur(img_predict[:,onset_index:offset_index],(21,21),0)
        #     res = cv2.medianBlur(img_predict[:,onset_index:offset_index], 19)
                    
            b_plane = res[:,:,0] & 0xF0 # B            
            g_plane = res[:,:,1] & 0xF0 # G            
            r_plane = res[:,:,2] & 0xF0 # R

            ### 1 ###
            b_mean = BitArray('0x{:0>2X}'.format(int(np.mean(b_plane)))).bin[0:4]
            g_mean = BitArray('0x{:0>2X}'.format(int(np.mean(g_plane)))).bin[0:4]
            r_mean = BitArray('0x{:0>2X}'.format(int(np.mean(r_plane)))).bin[0:4]
            _mean = b_mean+g_mean+r_mean

            b_max = BitArray('0x{:0>2X}'.format(int(np.max(b_plane)))).bin[0:4]
            g_max = BitArray('0x{:0>2X}'.format(int(np.max(g_plane)))).bin[0:4]
            r_max = BitArray('0x{:0>2X}'.format(int(np.max(r_plane)))).bin[0:4]
            _max = b_max+g_max+r_max

            b_min = BitArray('0x{:0>2X}'.format(int(np.min(b_plane)))).bin[0:4]
            g_min = BitArray('0x{:0>2X}'.format(int(np.min(g_plane)))).bin[0:4]
            r_min = BitArray('0x{:0>2X}'.format(int(np.min(r_plane)))).bin[0:4]
            _min = b_min+g_min+r_min

            b_med = BitArray('0x{:0>2X}'.format(int(np.median(b_plane)))).bin[0:4]
            g_med = BitArray('0x{:0>2X}'.format(int(np.median(g_plane)))).bin[0:4]
            r_med = BitArray('0x{:0>2X}'.format(int(np.median(r_plane)))).bin[0:4]
            _med = b_med+g_med+r_med
            
            chord_bit_string = _max

            # arr = [_mean,_min,_med]
            # a, cnts = np.unique(arr, return_counts=True)
            # high_freq, high_freq_element = cnts.max(), a[cnts.argmax()]
            # if (high_freq == 1 or high_freq == 3) and (int(high_freq_element,2) >= 322) and (int(high_freq_element,2) <= 4095):
            #     chord_bit_string = high_freq_element    

            print("[%d] <<%d>> [%d,%d,%d,%d]"%(num+1, int(chord_bit_string,2), int(_min,2), int(_mean,2), int(_med,2), int(_max,2)))        
            
            ### 2 ###
            # rgb_plane = b_plane << 16 + g_plane << 8 + r_plane         
            # RGB_unique, RGB_counts = np.unique(rgb_plane, return_counts=True)
            # RGB_dict = dict(zip(RGB_unique, RGB_counts))
            # sorted_RGB_dict = sorted(RGB_dict.items(), key=lambda x:x[1],reverse=True)
            # RGB = int(sorted_RGB_dict[0][0])
            # rgb_bit_string = BitArray('0x{:0>6X}'.format(int(RGB))) 
            # chord_bit_string = rgb_bit_string.bin[0:4] + rgb_bit_string.bin[8:12] + rgb_bit_string.bin[16:20]
            # print(num,b_max,g_max,r_max,rgb_bit_string.bin[0:4],rgb_bit_string.bin[8:12],rgb_bit_string.bin[16:20])

            ### 3 ###
            # B_unique, B_counts = np.unique(b_plane, return_counts=True)
            # B_dict = dict(zip(B_unique, B_counts))
            # sorted_B_dict = sorted(B_dict.items(), key=lambda x:x[1],reverse=True)
            # B = int(sorted_B_dict[0][0])
            # b_bit_string = BitArray('0x{:0>2X}'.format(B)) 

            # G_unique, G_counts = np.unique(g_plane, return_counts=True)
            # G_dict = dict(zip(G_unique, G_counts))
            # sorted_G_dict = sorted(G_dict.items(), key=lambda x:x[1],reverse=True)
            # G = int(sorted_G_dict[0][0])
            # g_bit_string = BitArray('0x{:0>2X}'.format(G)) 

            # R_unique, R_counts = np.unique(r_plane, return_counts=True)
            # R_dict = dict(zip(R_unique, R_counts))
            # sorted_R_dict = sorted(R_dict.items(), key=lambda x:x[1],reverse=True)
            # R = int(sorted_R_dict[0][0])
            # r_bit_string = BitArray('0x{:0>2X}'.format(R)) 
            # chord_bit_string = b_bit_string.bin[0:4]+g_bit_string.bin[0:4]+r_bit_string.bin[0:4]

            chord_int = int(chord_bit_string, 2)
            

            chord_row_index = df_symble.shape[0]-1
            if chord_int <= 4095 and chord_int >= 322:
                mod = chord_int%7
                mul = int((chord_int)/7)*7
                if mod >= 4:
                    mul = int((chord_int)/7+1)*7
                # print('mul',mul,mod,chord_int)
                _index_ = df_symble[df_symble.code.isin([mul])].index

                chord_row_index = int(_index_[0])
            # print(num,chord_int,chord_row_index)
            chord_symble = df_symble.iloc[int(chord_row_index)]['chord']
            
            # print (round(t[0],3),round(t[1],3),'\t\t',chord_symble,chord_row_index,chord_int)
            chord_note.append((t[0],t[1],chord_row_index))
        np.savez('%s/%d_chord_note.npz'%(out_path,song_id), note=chord_note)
        print('[chord2note2] Done ... %.3fs'%(ts.time() - startTime))