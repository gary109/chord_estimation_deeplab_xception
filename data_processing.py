"""
[CE200] General-purpose training script for image-to-image translation.
    [Modified] Sample Rate form 22050 to 20442
    =============================
     Song Total Sec: 201.375 sec
    =============================
    - 201.375 use 22050(sample rate) => 4440321
    - Featurse Map Length: 4440321 / 512(hop length) = 8673, So that pixel time is 0.0232 sec (201.375/8673)
    -----------------------------------------
    - 201.375 use 20442(sample rate) => 4116510
    - Featurse Map Length: 4116510 / 512(hop length) = 8040
    - So, pixel time is 0.025 sec 
    ====================================================================
    [STEP 0] For Ground truth node to image
      * python data_processing.py --rule chord2img --csv ./train.csv
    ====================================================================
    [STEP 1] For Train Datasets (generate train feature full png)
      * python data_processing.py --rule feature_full --csv ./train.csv
    ====================================================================
    [STEP 2] For AB Features Datasets (combine Domain A&B for pix2pix inputs form train's feature full png)
      * python data_processing.py --rule AB_features --csv ./train.csv --shift --time_steps 0.4 --output_root ./datasets --hFlip
    ====================================================================
    [STEP 3] For Train Datasets (spliting feature full png to Train/Val)
      * python data_processing.py --rule genTrainVal --test_size 0.001 --total_size 800000 --output_root ./datasets


by Gary Wang
"""
import os
import sys
import subprocess
import argparse
from pathlib import Path
from gutils import GUtils
import shutil
import json
import pandas as pd
from os import path

from multiprocessing import Pool
import multiprocessing as mp
import time
from datetime import datetime
import warnings
def ignore_warn(*args, **kwargs):
    pass
warnings.warn = ignore_warn

###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################          
def run_genAB(image_path_list):
    count = image_path_list[0]
    src_path = image_path_list[1]
    des_path = image_path_list[2]
    # dirname = os.path.dirname(des_path)
    # basename = os.path.basename(src_path).split('.',1)[0]
    # imgAB = cv2.imread(src_path)
    # imgA = imgAB[:,:256,:]
    # imgB = imgAB[:,256:,:]
    # save_A_path = '%s/%s_A.png'%(des_path,basename)
    # save_B_path = '%s/%s_B.png'%(des_path,basename)
    # cv2.imwrite(save_A_path, imgA)  
    # cv2.imwrite(save_B_path, imgB)  
    # os.remove(src_path)
    # print('[%d] A:%s <-> B:%s '%(count,save_A_path,save_B_path))

    print('[MOVE Train/Val Datasets] %s -> %s '%(src_path, des_path))
    shutil.move(src_path, des_path)

###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################          
def run_copy_train_file(__path):
    desPath = '/'.join((gutils.output_root, gutils.train_dir))
    print('[COPY Train Datasets] %s -> %s '%(__path, desPath))
    shutil.copy(__path, desPath)
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################           
def run_copy_val_file(__path):
    desPath = '/'.join((gutils.output_root, gutils.val_dir))
    print('[COPY Val Datasets] %s -> %s '%(__path, desPath))
    shutil.copy(__path, desPath )
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################           
def run_copy_test_file(__path):
    desPath = '/'.join((gutils.output_root, gutils.test_dir))
    print('[COPY Test Datasets] %s -> %s '%(__path, desPath))
    shutil.copy(__path, desPath )
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################          
def run_move_train_file(__path):
    desPath = '/'.join((gutils.output_root, gutils.train_dir))
    print('[MOVE Train Datasets] %s -> %s '%(__path, desPath))
    shutil.move(__path, desPath)
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################           
def run_move_val_file(__path):
    desPath = '/'.join((gutils.output_root, gutils.val_dir))
    print('[MOVE Val Datasets] %s -> %s '%(__path, desPath))
    shutil.move(__path, desPath )
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################           
def run_move_test_file(__path):
    desPath = '/'.join((gutils.output_root, gutils.test_dir))
    print('[MOVE Test Datasets] %s -> %s '%(__path, desPath))
    shutil.move(__path, desPath )
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################  
def run_output_AB_features_datasets(imgs_MP3_Note):
    gutils.run_output_AB_features_datasets(imgs_MP3_Note,hFlip=opt.hFlip)
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################  
def run_chord2img(song_groundtruth_path):
    gutils.run_chord2img(song_groundtruth_path)
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################  
def run_Creating_Datasets_ALLINONE(song_path):  
    gutils.run_Creating_Datasets_ALLINONE(song_path)
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################  
def run_Creating_feature_full_encoder(song_path):  
    gutils.run_Creating_feature_full_encoder(song_path)
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################  
def run_result2note(song_id):    
    note_list = gutils.result2note(song_id=song_id, result_dir=opt.result_dir)  
    return song_id, note_list

def _build_parser():
    parser = argparse.ArgumentParser()    
    parser.add_argument('--rule', required=True, help='AB_features, train, test, feature_full, feature_full_encode')
    parser.add_argument('--output_root', type=str, default='./datasets', help='path to images')
    parser.add_argument('--csv', default='train.csv', help='csv file (train.csv or test.csv)')
    parser.add_argument('--time_steps', type=float, default=0.1, help='time_steps for shift scale')
    parser.add_argument('--pixel_interval', type=float, default=0.025, help='time/pixel')
    parser.add_argument('--sample_rate', type=int, default=20442, help='sample_rate')    
    parser.add_argument('--test_size', type=float, default=0.25, help='splite datasets to train / val')
    parser.add_argument('--shift', action='store_true', help='shift time_steps in one song for features image')
    parser.add_argument('--renew_all', action='store_true', help='renew_all')
    parser.add_argument('--isTrain', action='store_true', help='isTrain')
    parser.add_argument('--hop_length', type=int, default=512, help='hop_length')
    parser.add_argument('--num_test', type=int, default=300000, help='num_test')
    parser.add_argument('--result_dir', type=str, default='./results/predict/images', help='result_dir')
    parser.add_argument('--eopch', type=str, default='latest', help='eopch')
    parser.add_argument('--total_size', type=int, default=10000, help='train/val/test total size')
    parser.add_argument('--startIndex', type=int, default=-1, help='stratIndex using for hooktheroy datasets')
    parser.add_argument('--endIndex', type=int, default=-1, help='endIndex using for hooktheroy datasets')  
    parser.add_argument('--datasetPath', type=str, default='./datasets', help='datasets path') 
    parser.add_argument('--avgRGB', type=float, default=-1.0, help='avgRGB<0 mean all pass, otherwise bypass smaller it')
    parser.add_argument('--hFlip', action='store_true', help='hFlip')
    return parser

def print_options(parser,opt):
    """Print and save options
    It will print both current options and default values(if different).
    It will save options into a text file / [checkpoints_dir] / opt.txt
    """
    message = ''
    message += '----------------- Options ---------------\n'
    for k, v in sorted(vars(opt).items()):
        comment = ''
        default = parser.get_default(k)
        if v != default:
            comment = '\t[default: %s]' % str(default)
        message += '{:>25}: {:<30}{}\n'.format(str(k), str(v), comment)
    message += '----------------- End -------------------'
    print(message)
            
if __name__ == '__main__':
    parser = _build_parser()
    opt, _ = parser.parse_known_args()
    
    print_options(parser,opt)
    utils = Utils()
    gutils.init_vars(shift=opt.shift, hop_length=opt.hop_length, 
                    sample_rate=opt.sample_rate, time_steps=opt.time_steps, 
                    pixel_interval=opt.pixel_interval, isTrain=opt.isTrain,
                    renew_all=opt.renew_all)
    gutils.init_datasets(opt.output_root)
##################################################################################################################################################   
##################################################################################################################################################
##################################################################################################################################################
    if opt.rule == 'AB_features':
        startTime = time.time()
        train = pd.read_csv(opt.csv)
        imgs_MP3_Note = []
        ###################################################################################################
        for _path, _groundtruth, _songs in zip(train['paths'],train['groundtruth'],train['songs']):
            mp3_path = os.path.join(_path, _songs)
            basename = os.path.basename(mp3_path)
            imgname = '%s_feature_full.png'%(os.path.splitext(basename)[0])
            mp3_img_path = os.path.join(_path, imgname)

            groundtruth_path = os.path.join(_path, _groundtruth)
            basename = os.path.basename(groundtruth_path)
            imgname = '%s_%s.png'%(os.path.splitext(os.path.basename(mp3_path))[0],os.path.splitext(basename)[0])
            groundtruth_img_path = os.path.join(_path, imgname)

            if path.exists(mp3_img_path) == True and path.exists(groundtruth_img_path) == True:        
                print(mp3_img_path, groundtruth_img_path)
                imgs_MP3_Note.append((mp3_img_path, groundtruth_img_path))
            else:
                print(" Not found MP3(%s) or groundtruth(%s)"%(mp3_img_path,groundtruth_img_path))
        ###################################################################################################
        try:
            shutil.rmtree(gutils.feature_file_path)
        except OSError as e:
            print(e)
        else:
            print("The %s directory is deleted successfully"%(gutils.feature_file_path))
        os.makedirs('%s' % (gutils.feature_file_path), exist_ok=True)    
        gutils.delete_ipynb_checkpoints()
                
        ###################
        # Muti-processing #
        ###################
        pool_size = mp.cpu_count()
        print('pool size:%d'%(pool_size))
        pool = Pool(processes=pool_size)
        pool.map(run_output_AB_features_datasets, imgs_MP3_Note)
        pool.close()  
        pool.join()     
        print('Finishied %fs'%(time.time() - startTime))  

##################################################################################################################################################   
##################################################################################################################################################
##################################################################################################################################################
    elif opt.rule == 'genTrainVal':
        import cv2
        import secrets
        import numpy as np
        import copy
        from glob import glob
        import time
        import shutil
  
        test_size=opt.test_size
        total_size=opt.total_size
        gutils.secretsGenerator = secrets.SystemRandom()
        startTime = time.time()
        imageA_path_list = []
        imageB_path_list = []

        print('=== Glob A&B in feature_file_path ===',gutils.feature_file_path)
        for file_path in glob('%s/*_A.png'%(gutils.feature_file_path)):
            imageA_path_list.append(file_path)

        for file_path in glob('%s/*_B.png'%(gutils.feature_file_path)):
            imageB_path_list.append(file_path)

        assert (len(imageA_path_list) == len(imageB_path_list))

        imageA_path_list.sort()
        imageB_path_list.sort()

        if len(imageA_path_list) < total_size:
            total_size = len(imageA_path_list)

        _train_size = int(total_size*(1-test_size))
        _val_size = int(total_size-_train_size)

        print(_train_size,_val_size)

        # gutils.secretsGenerator.shuffle(image_path_list)

        trainA_list = imageA_path_list[:_train_size]
        valA_list = imageA_path_list[_train_size:] 

        trainB_list = imageB_path_list[:_train_size]
        valB_list = imageB_path_list[_train_size:] 


        print('=== Clean Train/Val Datasets ===')
        path_list = [gutils.train_file_path , gutils.val_file_path]
        for pa in path_list:
            try:
                shutil.rmtree(pa)
            except OSError as e:
                print(e)
            finally:
                print("The %s directory is deleted successfully"%(pa))
            os.makedirs('%s' % (pa), exist_ok=True)
   
        gutils.delete_ipynb_checkpoints()

        all_img_list = []
        # For Train
        count = 1
        for tA,tB in zip(trainA_list,trainB_list):
            print('[%d] '%(count),tA,tB)
            all_img_list.append((count,tA,gutils.train_file_path))
            all_img_list.append((count,tB,gutils.train_file_path))
            count += 1

        # For Val
        for tA,tB in zip(valA_list,valB_list):
            print('[%d] '%(count),tA,tB)
            all_img_list.append((count,tA,gutils.val_file_path))
            all_img_list.append((count,tB,gutils.val_file_path))
            count += 1       
        
        # Muti-processing
        print('### [%s] Creating Train/Val Datasets For DeepLab ###'%(gutils.output_root))  
        pool_size = mp.cpu_count()
        print('pool size:%d'%(2))
        pool = Pool(processes=2) # Pool() 不放參數則默認使用電腦核的數量  
        pool.map(run_genAB, all_img_list)
        
        pool.close()  
        pool.join()  


        # Check Train/Val A&B pair
        print('### Check Train A&B pair ###') 
        A=[]
        B=[]
        for file_path in glob('%s/*_A.png'%(gutils.train_file_path)):
            A.append(file_path)
        for file_path in glob('%s/*_B.png'%(gutils.train_file_path)):
            B.append(file_path)
        assert (len(A) == len(B))

        print('### Check Val A&B pair ###') 
        A=[]
        B=[]
        for file_path in glob('%s/*_A.png'%(gutils.val_file_path)):
            A.append(file_path)
        for file_path in glob('%s/*_B.png'%(gutils.val_file_path)):
            B.append(file_path)
        assert (len(A) == len(B))



        print("[%s] Row image size:"%(gutils.feature_file_path), len(trainA_list)+len(trainB_list)+len(valA_list)+len(valB_list))
        print("[%s] train:%d val:%d"%(gutils.output_root, len(train_list), len(val_list)))
        print('=== Finishied %fs ==='%(time.time() - startTime)) 
##################################################################################################################################################   
##################################################################################################################################################
##################################################################################################################################################
    elif opt.rule == 'test':
        gutils.output_test_datasets(csvFile=opt.csv)
##################################################################################################################################################   
##################################################################################################################################################
##################################################################################################################################################
    elif opt.rule == 'chord2img':    
        startTime = time.time()
        # gutils.chord2img(opt.csv)   
        song_groundtruth_path = []
        train = pd.read_csv(opt.csv)
        count = 1
        for _path, _groundtruth, _songs in zip(train['paths'],train['groundtruth'],train['songs']):
            mp3_path = os.path.join(_path, _songs)
            groundtruth_path = os.path.join(_path, _groundtruth)
            if path.exists(mp3_path) == True:
                if path.exists(groundtruth_path) == True:   
                    song_groundtruth_path.append((_path, mp3_path, groundtruth_path))
                else:
                    print(" Not found Groundtruth %s"%(groundtruth_path))
            else:
                print(" Not found MP3 %s"%(mp3_path))  

        ###################
        # Muti-processing #
        ###################
        pool_size = mp.cpu_count()
        print('pool size:%d'%(10))
        pool = Pool(processes=10)   
        pool.map(run_chord2img, song_groundtruth_path)
        pool.close()  
        pool.join()     
        print('Finishied %fs'%(time.time() - startTime))  

##################################################################################################################################################   
##################################################################################################################################################
##################################################################################################################################################
    elif opt.rule == 'feature_full':             
        startTime = time.time()
        data = pd.read_csv(opt.csv)              
        files_MP3 = []
        for _path, _songs in zip(data['paths'],data['songs']):
            mp3_path = os.path.join(_path, _songs)
            if path.exists(mp3_path) == True:  
                files_MP3.append(mp3_path)          

        ###################
        # Muti-processing #
        ###################
        pool_size = mp.cpu_count()
        print('pool size:%d'%(10))
        pool = Pool(processes=10) 
        pool.map(run_Creating_Datasets_ALLINONE, files_MP3)
        pool.close()  
        pool.join()     
        print('Finishied %fs'%(time.time() - startTime))       

##################################################################################################################################################   
##################################################################################################################################################
##################################################################################################################################################
    elif opt.rule == 'feature_full_encode':             
        startTime = time.time()
        data = pd.read_csv(opt.csv)              
        files_MP3 = []
        for _path, _songs in zip(data['paths'],data['songs']):
            mp3_path = os.path.join(_path, _songs)
            if path.exists(mp3_path) == True:  
                files_MP3.append(mp3_path)          

        ###################
        # Muti-processing #
        ###################
        pool_size = mp.cpu_count()
        print('pool size:%d'%(10))
        pool = Pool(processes=10) 
        pool.map(run_Creating_feature_full_encoder, files_MP3)
        pool.close()  
        pool.join()     
        print('Finishied %fs'%(time.time() - startTime))      

##################################################################################################################################################   
##################################################################################################################################################
##################################################################################################################################################
    elif opt.rule == 'result2note':
        startTime = time.time()
        
        ###################
        # Muti-processing #
        ###################
        pool_size = mp.cpu_count()
        print('pool size:%d'%(pool_size))
        all_note_dict = {}
        songs_id = [x for x in range(1,1501)] 
        pool = Pool(processes=5)   
        s_id, s_list = zip(*pool.map(run_result2note, songs_id))     
        pool.close()  
        pool.join()     
        
        ###############
        # Output json #
        ###############
        all_note_dict = {}
        for _id, _list in zip(s_id, s_list):
            all_note_dict['%d'%(_id)] = _list  
        datestr = datetime.now().strftime('%Y_%m_%d_%H_%M_%S')
        with open('result-%s.json'%(datestr), 'w') as outfile:
            json.dump(all_note_dict, outfile)              
        print('Finishied %fs'%(time.time() - startTime))

    sys.exit(0)
    
    